<?php
/**
 * @category WebPT
 * @copyright Copyright (c) 2013 WebPT, INC
 * @author jgiberson
 * 12/2/13 5:46 PM
 */

namespace Wpt\SOAP\Save\LegacyHarness;

use Addendum;
use Appointment;
use Assert\AssertionFailedException;
use Assessment;
use Authorization;
use BillingSheet;
use Clinic;
use ClinicModule;
use ClinicPreference;
use dashAlerts;
use EMRICDPortable\Marshallers\Marshallers;
use EMRICDPortable\Model\IcdModelInterface;
use ErrorMessage;
use individualTest;
use IsHtmlErrorMessage;
use Logger;
use Objective;
use Patient;
use PatientBilling;
use PatientPrecautions;
use PDF;
use Plan;
use PQRI;
use PQRIMeasure;
use PQRIProvider;
use PTCase;
use QuickDischargeBuilderUser;
use QuickDischargePdfBuilder;
use QuickDischargePdfBuilderAppointment;
use QuickDischargeSaveBuilder;
use SaveSoapException;
use Soap;
use Subjective;
use T436;
use TestFactory;
use Therapist;
use WebOutcomes\SOAP\Test\Omt\Validator\ValidatorInterface;
use WebOutcomes\SOAP\Test\Omt\WebOutcomesService;
use WebOutcomes\SOAP\Test\Omt\WebOutcomesServiceWithMipsSupportInterface;
use Webpt\AppTherapist\LegacyCompatibility\TherapistInterface;
use WebPT\Mips\Soap\Validators\Dto\MipsData;
use Wpt\Appointment\Diagnosis\Diagnosis;
use Wpt\Cache\Entity\Appointment as AppointmentEntity;
use Wpt\Cache\Entity\CaseHasAppointmentOnFacility;
use Wpt\Cache\Entity\Legacy\Appointment as LegacyAppointmentCacheEntity;
use Wpt\Common\Services\ProcessorInterface;
use Wpt\Config\ConfigInterface;
use Wpt\DigitalPatientIntake\Dto\DigitalPatientIntake;
use Wpt\FeatureFlip\FeatureFlipInterface;
use Wpt\FeatureFlip\SimpleArrayFeatureFlip;
use Wpt\FeatureFlipConstant;
use Wpt\Foto\Dto\FotoVerifyDto;
use Wpt\Foto\Foto;
use Wpt\Foto\Visit;
use Wpt\Foto\Discharge;
use Wpt\FunctionalLimitation\Medicare\AddendumRequired\Service as AddendumRequiredService;
use Wpt\Legacy\Util;
use Wpt\Mips\Soap\DTO\MIPSValidationContext;
use Wpt\Mips\Validators\Soap\MipsValidatorsFactory;
use Wpt\Modifiers\Services\ModifiersLimitAmount;
use Wpt\Modifiers\Services\ModifiersLimitAmountInterface;
use Wpt\Patient\Icd\RevisionEnum;
use Wpt\Patient\Icd\Validator as IcdValidator;
use Wpt\Persistence\Payer\PayerByByPiidDao;
use Wpt\Persistence\Payer\PayerByPiidDao;
use Wpt\Persistence\Payer\PayerByPiidDaoInterface;
use Wpt\Persistence\Payer\PayerByPiidProxy;
use Wpt\Persistence\Payer\PayerDao;
use Wpt\PQRS\Dto\RecordablePqrsDataDto;
use Wpt\PTCase\Chart\PatientRecords\ManagePatientIntake\DigitalPatientIntakeUpdater;
use Wpt\Scheduler\Agenda;
use Wpt\Service\Policy\PolicyAuthorizationInterface;
use Wpt\Service\PQRS\Period\FirstPqriPeriodInterface;
use Wpt\Service\SOAP\Event\Emitter\CaseNoteAppointmentInterface;
use Wpt\Service\SOAP\Event\Emitter\CaseNoteAppointmentService;
use Wpt\Service\SOAP\Event\Emitter\FinalizePatientNoteEventEmitterInterface;
use Wpt\Service\SOAP\Event\Emitter\AppointmentProviderInterface;
use Wpt\Service\SOAP\Event\Emitter\AppointmentProvider;
use Wpt\Snapshot;
use Wpt\Persistence\DaoFactory;
use Wpt\Persistence\Appointment\AddendumDao;
use Wpt\Service\PQRS\BackPainService;
use Wpt\Exception\SnapshotException;
use Wpt\Feeschedule\Medicare\FunctionalLimitations\Manager;
use Wpt\FunctionalLimitation\Medicare\Service as FlrMedicareService;
use Wpt\Cache\Entity\Manager as CacheEntityManager;
use Exception;
use Wpt\SOAP\Billing\BillableNote\BillableNoteListener;
use Wpt\SOAP\Billing\BillableNote\PostSaveEventListenerFactory;
use Wpt\SOAP\COCQ\Service\CoCqBillingService;
use Wpt\SOAP\COCQ\Service\CoCqBillingServiceInterface;
use Wpt\SOAP\Dto\Iroms\SoapIromsExclusionException;
use Wpt\SOAP\Event\SoapNoteIdProvider;
use Wpt\SOAP\Exception\DateOfService\FutureDate;
use Wpt\SOAP\Exception\DateOfService\NullDate;
use Wpt\SOAP\Preview\Finalize\Dto\Finalize as PreviewFinalizeDto;
use Wpt\SOAP\Preview\Finalize\Finalize as PreviewFinalizeService;
use Wpt\SOAP\Save\Dto\Request;
use Wpt\SOAP\Save\FlrGcodesCollector;
use Wpt\SOAP\Save\LegacyHarness\Dto\LegacyHarnessContextDto;
use Wpt\SOAP\Save\LegacyHarness\Service\AutoSaveVerifier;
use Wpt\SOAP\Save\LegacyHarness\Service\AutoSaveVerifierInterface;
use Wpt\SOAP\Save\LegacyHarness\Service\BillingSheetValidatorChainFactory;
use Wpt\SOAP\Save\LegacyHarness\Service\ScheduledAppointmentValidator;
use Wpt\SOAP\Save\LegacyHarness\Service\QuickDischargePdfCreatorInterface;
use Wpt\SOAP\Save\LegacyHarness\Service\LegacyHarnessSaveHelperInterface;
use Wpt\SOAP\Services\AppointmentHasPayer\AppointmentHasPayerService;
use Wpt\SOAP\Services\AppointmentHasPayer\AppointmentHasPayerServiceInterface;
use Wpt\SOAP\Services\Iroms\IromsExclusionPersistenceInterface;
use Wpt\SOAP\Services\Iroms\IromsExclusionPersistenceService;
use Wpt\SOAP\Services\SoapValidationErrorInterface;
use Wpt\SOAP\Services\SoapValidationErrorService;
use Wpt\SOAP\SoapPdfRenderer;
use Wpt\SOAP\SoapUrlProvider;
use Wpt\SOAP\Subjective\Dto\SubjectiveSurgeryContextDto;
use Wpt\SOAP\Test\SoapTestCacheCleanupInterface;
use Wpt\SOAP\Validators\Iroms\IromsExclusionRequiredValidator;
use Wpt\SOAP\Validators\Surgery\SoapFinalizationSurgeryValidator;
use Wpt_Cache;
use Wpt_Cache2;
use Wpt_Config;
use Wpt_Patient_Prescription;
use Wpt\Poc\PlanOfCare;
use wptSession;
use Wpt\Service\SOAP\SoapService;
use ErrorMessageTab;
use Wpt\Patient\PNCountdown;
use Wpt\Zend\ServiceManager\StaticServiceManager;
use Zend\EventManager\Event;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventsCapableInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;
use \Wpt\SOAP\Save\LegacyHarness\Service\SoapValidatorInterface;
use Zend\Validator\ValidatorInterface as ZendValidatorInterface;

/**
 * Class LegacyHarness
 * @package Wpt\SOAP\Save
 * This class provides a testable harness for the current legacy save SOAP code.
 * Code was pulled from saveSOAP.php and placed in this class to begin providing tests for the save soap
 * process. References to environment variables (like request data) and calls to static class methods
 * have been replaced by method parameters or wrapped with protected function calls. This allows during testing
 * for all objects to be stubbed or mocked so expectations can be specified.
 */
class LegacyHarness implements EventsCapableInterface
{
    const SAVE_SOAP_CHECKSUM_TTL = 'performance_settings/Wpt/SOAP/Save/LegacyHarness/TTL';
    const SOAP_NOTE_PDF_FINALIZED = 'Emr.Documentation.SoapNotePdfFinalized';

    /** @var PolicyAuthorizationInterface */
    private $policyAuthorization;

    /** @var  Wpt_Cache */
    protected $cache;

    /** @var  Wpt_Config */
    protected $config;

    /** @var  wptSession */
    protected $session;

    /** @var  CacheEntityManager */
    protected $cacheEntityManager;

    /** @var  int */
    protected $pqrsCacheEnabled;

    /** @var  int */
    protected $pqrsCacheTtl;

    /** @var  Logger */
    protected $logger;

    /** @var ServiceManager */
    protected $serviceManager;

    /** @var  EventManagerInterface */
    private $eventManager;

    /** @var AutoSaveVerifierInterface */
    private $autoSaveVerifierService;

    /** @var Patient */
    private $patientService;

    /** @var FeatureFlipInterface */
    private $featureFlip;
    
    /** @var ValidatorInterface */
    private $mipsValidator;

    /**
     * @var SoapTestCacheCleanupInterface
     */
    private $soapTestCacheCleanupService;

    /**
     * @var ZendValidatorInterface
     */
    private $soapFinalizationSurgeryValidator;

    /**
     * @var SoapValidationErrorInterface
     */
    private $soapValidationErrorService;

    /**
     * @var FinalizePatientNoteEventEmitterInterface
     */
    private $finalizePatientNoteEventEmitter;

    /**
     * @var ProcessorInterface
     */
    private $dpiUpdater;

    /**
     * @var CoCqBillingServiceInterface
     */
    private $coCqBillingService;

    /**
     * @var ZendValidatorInterface
     */
    private $iromsExclusionRequiredValidator;

    /**
     * @var IromsExclusionPersistenceInterface
     */
    private $iromsExclusionPersistenceService;

    /**
     * @var ZendValidatorInterface
     */
    private $iromsExclusionValidator;

    /**
     * @var AppointmentHasPayerServiceInterface
     */
    private $appointmentHasPayerService;

    /**
     * The single entry point to save soap, all of the environment variables should be provided through parameters
     * of this function. Any references to environment variables should be replaced with parameter references.
     * @param Request $request
     * @throws Exception
     * @throws SnapshotException|AssertionFailedException
     */
    public function save(Request $request)
    {
        if ($this->getFeatureFlip()->enabled(FeatureFlipConstant::DN_NOT_GENERATED_WITH_IE)) {
            set_time_limit(180);
        }
        $this->getFlrGcodesCollector()->setLogValues(
            __FUNCTION__,
            [
                'myAptId' => $request->getMyAptId(),
                'appointmentType' => $request->getAppointmentType(),
                'isIncludeDailyNote_1' => $request->isIncludeDailyNote(1),
                'isIncludeDailyNote_true' => $request->isIncludeDailyNote(true),
            ]
        );
        $requestData = $request->getRequest()->toArray();
        $postData = $request->getPost()->toArray();
        $queryData = $request->getQuery()->toArray();
        $objReadOnlyDatabase = $request->getReader();

        // needs to be global, so it can be used by subjective
        global $myUser;
        global $mySession;
        global $myCP;
        global $myClinic;

        $myAppointment = null;
        $CaseID = null;
        $xhrRequest = null;
        $errDump = null;

        $mySession = $this->getSessionInstance();
        $mySession->checkSession ();
        $logger = $this->getLoggerInstance();
        $logger2 = $this->getLogger();
        
        $isAutoSave = $request->isAutoSave();

        if ($this->hasChangeFound($mySession->getId(), $request) == AutoSaveVerifier::AUTOSAVE_HAS_NO_CHANGES_FOUND) {
            header('Content-type: application/json');
            echo json_encode([
                'error' => 'NO_CHANGES_FOUND',
            ]);

            $this->logSoapEnd();
            $logger->info('Skipping auto save because no changes were detected.');
            exit;
        }

        $myCP = $this->getClinicPreferenceByFacilityId($mySession->FacilityID);
        $myMods = $this->getClinicModulesByFacilityId($mySession->FacilityID);
        $myUser = $this->getTherapistById($mySession->UserID);

        //Grabs all the data from $postData and throws it in variables used for storing the note.
        $booIncludeDailyNote = $request->isIncludeDailyNote(true) || $request->isIncludeDailyNote(1);

        $AptID = 0;
        $addn = 0;
        $AptType = $request->getAppointmentType() ? $request->getAppointmentType() : $mySession->AptType;
        $PatientID = $request->getPatientId();

        //really big try catch but its what we need to work with
        try
        {
            if ($request->hasMyAptId())
            {
                $AptID = intval($request->getMyAptId());
                if($AptID != 0){
                    $myAppointment = ($AptID > 0) ? $this->getAppointmentById($AptID) : $this->getAppointmentByAddendumId($AptID);
                }
                if($myAppointment == null){
                    throw new SaveSoapException(array('error' => "APT_NOT_FOUND", 'patientID' => $PatientID, 'caseID' => $CaseID), "patientChart.php?ID={$PatientID}&msg=17");
                }

	            $myClinic = $this->getClinicByFacilityId($myAppointment->getFacilityId());
                if ($myAppointment->PatientID != $PatientID) {
                    throw new SaveSoapException(array(), "dashboard.php");
                } elseif (0 < $myAppointment->DocID) {
                    throw new SaveSoapException(array('error' => "DOC_FINALIZED", 'patientID' => $PatientID, 'caseID' => $myAppointment->CaseID), "patientChart.php?ID={$myAppointment->PatientID}&CaseID={$myAppointment->CaseID}&msg=12");
                } else {
                    $NewNoteDetected = false;
                    $CaseID = $myAppointment->getCaseID();
                }
            } else {
	            $myClinic = $this->getClinicByFacilityId($mySession->FacilityID);
                $NewNoteDetected = true;
                $CaseID = $request->getCaseId();
            }

            if ($request->hasAddn())
            {
                $addn = $request->getAddn();
                if ($request->isSubmittedSet())
                {
                    $myAppointment = $this->getAppointmentById($addn);
                    $myAppointment->setAptAddnFlag($addn);
                }else{
                    $myAppointment = $this->getNewAppointmentInstance();
                }

                $myAddendum = $this->getNewAddendumInstance();
                $myAddendum->AptID = $addn;
                $AptID = $addn;
                $myAppointment->setAddendum($myAddendum);
            }

            if(empty($CaseID)){
                $CaseID = intval($request->getCaseId()) ? intval($request->getCaseId()) : null;
            }

            /** @var ErrorMessage[] $error */
            $error = array ();

            // If this isn't a new note being created, ensure that the user hasn't changed facilities in another window or tab before finalizing/saving this note.
            // This will ensure that the note is not created in the wrong facility.
            if(!$NewNoteDetected){
                $appointmentFacilityId = $myAppointment->getFacilityId();
                if($mySession->FacilityID != $appointmentFacilityId){
                    $redirectUrl = $isAutoSave ? null : 'dashboard.php';
                    $errorMessage = $isAutoSave ? 'INVALID_CLINIC' : "The active facility has been changed in another tab or window.";
                    throw new SaveSoapException( array('error' => $errorMessage), $redirectUrl);
                }
            }

            if(empty($PatientID) && $this->getFeatureFlip()->enabled('lam-b27999-log-post-body-if-patient-id-missing')) {
                $this->logger->error('Missing PatientID, POST Body: ' . file_get_contents("php://input"));
            }

            $patient = $this->loadPatient($PatientID, $mySession->FacilityID, $CaseID);
            if (!$patient->getId()) {
                throw new SaveSoapException([
                    'error' => 'Failed to load patient.',
                    'facilityId' => $mySession->getFacilityID(),
                    'userId' => $mySession->getUserId(),
                    'patientId' => $PatientID,
                    'caseId' => $CaseID,
                ], '/patient/display/');
            }

            $patient->loadLastApt ();

            if ($CaseID < 1) {
                throw new SaveSoapException(array(
                    'error' => 'SaveSOAP CaseID not set',
                    'facilityId' => $mySession->getFacilityID(),
                    'userId' => $mySession->getUserId(),
                    'patientID' => $PatientID,
                    'caseID' => $CaseID,
                    'REQUEST_CaseID' => $request->getCaseId()
                ), sprintf('patientChart.php?ID=%s', $PatientID));
            }
            $myPTCase = $this->getNewPTCaseInstance();
            $myPTCase->load ( $CaseID );
            $myPTCase->setPatient($patient);
            $patient->setCase($myPTCase);

            if ( ! $myPTCase->canAddAppointmentByType( $AptType)) {
                throw new SaveSoapException( array('error' => PTCase::ERROR_APPOINTMENT_GROUP), null);
            }

            $Medicare = $patient->isMedicare();
            $Medicaid = $patient->isMedicaid();
            $Pediatric = $patient->Pediatrics;

            //Remove clerical clinic Incomplete Documents cache
            $strDashboardAtGlanceTagClerical = 'DASHBOARD_ATAGLANCE_CLERICAL_'. $mySession->FacilityID;
            $this->getWptCacheInstance()->clearCache ( $strDashboardAtGlanceTagClerical );

            Agenda::getInstance()->deleteAgendaCacheByCaseId($patient->CaseID, $mySession->FacilityID);

            //If it's a pre-existing note and has an AptID
            if (($AptID && $myAppointment->UserID) || $addn) {
                $myAppointment->UserID = $mySession->UserID;
                $myAppointment->TimeIn = Util::canonicalize_time($request->getTimeIn());
                $myAppointment->TimeOut = Util::canonicalize_time($request->getTimeOut());
            } else { //Otherwise it's a new note
                $dnAndIEArentLinkedFF = $this->isDnAndIeAreLinkingEnabled();
                if ($dnAndIEArentLinkedFF) {
                    if (!$isAutoSave) {
                        $myAppointment = new Appointment();
                        $myAppointment->AptType = $AptType;
                        $myAppointment->Date = date("Y-m-d");
                        $myAppointment->PatientID = $PatientID;
                        $myAppointment->UserID = $mySession->UserID;
                        $myAppointment->TimeIn
                            = Util::canonicalize_time($request->getTimeIn());
                        $myAppointment->TimeOut
                            = Util::canonicalize_time($request->getTimeOut());
                        $myAppointment->CaseID = $patient->CaseID;
                        $myAppointment->CoSigner = $request->getMyCoSigner();
                        $myAppointment->store();
                    }
                } else {
                    $myAppointment = new Appointment();
                    $myAppointment->AptType = $AptType;
                    $myAppointment->Date = date("Y-m-d");
                    $myAppointment->PatientID = $PatientID;
                    $myAppointment->UserID = $mySession->UserID;
                    $myAppointment->TimeIn
                        = Util::canonicalize_time($request->getTimeIn());
                    $myAppointment->TimeOut
                        = Util::canonicalize_time($request->getTimeOut());
                    $myAppointment->CaseID = $patient->CaseID;
                    $myAppointment->CoSigner = $request->getMyCoSigner();
                    $myAppointment->store();
                }
                /**
                 * Apt ID stored into session for new notes ability to redirect on browser
                 * back button or refresh which would cause duplicate notes.
                 * @var int
                 */
                $mySession->NoteLastAptID = $myAppointment->AptID;
            }

            //Initialize PQRI related ids.  If there was an existing record then the myAppointment object's OriginalAptID will be set.
            $pqriAddn = 0;
            $originalAptID = (0 !== $myAppointment->OriginalAptID ? $myAppointment->OriginalAptID : $AptID);

            //If this is an addendum
            if ($addn) {
                //Build the Addendum object and store it
                $myAddn = new Addendum ();
                $myAddn->setAptType($AptType);
                $myAddn->setDate(date ( "Y-m-d" ));
                $myAddn->setPatientID($PatientID);
                $myAddn->setUserID($mySession->getUserId());
                $myAddn->setTimein(Util::canonicalize_time($request->getTimeIn()));
                $myAddn->setTimeout(Util::canonicalize_time($request->getTimeOut()));
                $myAddn->setAptID($AptID);
                $myAddn->setAddnID(0);
                $myAddn->store ();

                $AptID = - $myAddn->getAddnID();
                $addn = $myAddn->getAddnID();
                $myAppointment = Appointment::getByAddendumId($myAddn->getAddnID());
                $eventEmitter = $this->getSoapEventEmitter();
                $eventEmitter->emit($myAppointment, $myPTCase);
                if($Medicare){
                    $this->saveAddendumRequiredAddendumId($myAppointment);
                }
            } else {
                $AptID = $myAppointment->AptID; //this way the aptid gets bound to each object
            }

            $this->getFlrGcodesCollector()->setAppointmentId($AptID);
            //by this point we should always have a appointment
            if( empty($myAppointment)) {
                throw new SaveSoapException( array('error' => 'Appointment failed to load in SaveSoap.'), null);
            }
            {
                $logger2->debug("AptID [{$myAppointment->getAppointmentId()}] was loaded successfully.");
            }
            $myAppointment->setUser($myUser);
            $myAppointment->setCase($myPTCase);
            $myAppointment->setPatient($patient);

            // Initialize the manager for the request
            $this->getFunctionalLimitationsManagerInstance()->init($myAppointment);

            //if it is a new note be sure to set the facility id.
            if(true === $NewNoteDetected){
                //else use original notes facility
                if ($request->isValidOriginalAppointmentId())
                {
                    $myOriginalApt = new Appointment();
                    $myOriginalApt->load((int)$request->getOriginalAppointmentId());
                    $myAppointment->setFacilityID($myOriginalApt->getFacilityId());
                }
                //Set the facility id to what is in session, only when there is no original apt
                else
                {
                    $myAppointment->setFacilityID($mySession->getFacilityID());
                }
            }

            $pqriAptID = $AptID;
            if (0 > $pqriAptID) {
                /* if the appointment id is negative then this is an addendum id.  We only want to associate
                 * PQRI data with the original appointment so we need to get the original id from the Appointment object. */
                $pqriAddn = - $pqriAptID; /* invert the id */
                $pqriAptID = $originalAptID;
            }
            $mySubjective = $myAppointment->getSubjective();
            if ( empty( $mySubjective)) {
                $mySubjective = $this->createSubjectiveFromAppointment($myAppointment);
            }
            $mySubjective->setHasDailyNote($booIncludeDailyNote);
            $mySubjective->loadSOAPIDFromAptID ( $AptID );
            $mySubjective->PatientID = $PatientID;
            // need to bind now before Plan is instantiated to make isSpeech detection work.
            if (! $mySubjective->bind ( $postData )) {
                $error [] = new ErrorMessage ( "unable to bind subjective data" );
            }
            $logger2->debug("Bound subjective data to AptID [{$myAppointment->getAppointmentId()}].");

            // Loop trough Icd codes trying to find incomplete codes
            if (!$request->isSubmittedDraft())
            {
                /** @var Marshallers $marshaller */
                $marshaller = $this->getServiceManager()->get('Wpt.Patient.Icd.Marshaller');
                $dateOfService = $requestData['DateYear'] . '-' . $requestData['DateMonth'] . '-' . $requestData['DateDay'];
                $icdCodesModels = $marshaller->stringOrXmlToIcdModels($mySubjective->Diagnosis, $dateOfService);
                $icdTreatCodesModels = $marshaller->stringOrXmlToIcdModels($mySubjective->getTreatmentDiag(), $dateOfService);

                $icdCodes = json_decode($requestData['icdcodes'], true);
                $icdTreatCodes = json_decode($requestData['icdtreatcodes'], true);
                $icdMergedCodes = array_merge($icdCodesModels->getElements(), $icdTreatCodesModels->getElements());

                /** @var IcdModelInterface $icdCode */
                foreach ($icdMergedCodes as $icdCode)
                {
                    if (!$icdCode->isBillable())
                    {
                        $error[] = new ErrorMessage('Your ICD-10 code(s) is incomplete. Please find a billable code to finalize.');
                        break;
                    }
                }

                /** @var IcdValidator $icdValidator */
                $icdValidator = StaticServiceManager::getInstance()->get('Wpt.Patient.Icd.Validator');
                $diagnosisValidPrimaries = $icdValidator->validatePrimary($icdCodes);
                $treatmentValidPrimaries = $icdValidator->validatePrimary($icdTreatCodes);
                $icd9ValidPrimaries = $diagnosisValidPrimaries[RevisionEnum::ICD9] && $treatmentValidPrimaries[RevisionEnum::ICD9];
                $icd10ValidPrimaries = $diagnosisValidPrimaries[RevisionEnum::ICD10] && $treatmentValidPrimaries[RevisionEnum::ICD10];

                if (!$icd9ValidPrimaries && !$icd10ValidPrimaries) {
                    $error[] = new ErrorMessage('Please select a primary code for each ICD-9 and ICD-10');
                } elseif (!$icd10ValidPrimaries) {
                    $error[] = new ErrorMessage('Please select an ICD-10 primary code');
                } elseif (!$icd9ValidPrimaries) {
                    $error[] = new ErrorMessage('Please select an ICD-9 primary code');
                }
            }

            $myObjective = $this->getObjectiveByAppointment($myAppointment);
            $myObjective->PatientID = $PatientID;
            $arrTestNames = explode(",", $request->getArrTests());
            $myAppointment->DraftArray = implode($arrTestNames);
            //	INCLUDED WHEN BILLING SHEET IS SELECTED
            if ( $booIncludeDailyNote ) {
                $tmp = explode ( ",", $request->getArrTestsBilling());
                $arrTestNames = array_merge ( $arrTestNames, $tmp );
                $myAppointment->setIncludesDailyNote(true);
            }

            // Need to make sure CPT modifer value is set since value will not be posted if checkbox is not checked
            if( ! $request->isObjectiveApplyCptModifierSet())
            {
                $postData['ObjectiveApplyCPTModifier'] = '';
            }
            if (! $myObjective->bindPost ( $postData, $arrTestNames )) {
                $error [] = new ErrorMessage ( "unable to bind Objective data" );
            }
            $logger2->debug("Bound objective data to AptID [{$myAppointment->getAppointmentId()}].");

            $myAssessment = $myAppointment->getAssessment();
            if ( empty( $myAssessment)) {
                $myAssessment = $this->createAssessmentFromAppointment($myAppointment);
            }
            $myAssessment->loadSOAPIDFromAptID ( $AptID );
            $myAssessment->PatientID = $PatientID;
            if (! $myAssessment->bind ( $postData )) {
                $error [] = new ErrorMessage ( "unable to bind Assessment data" );
            }
            $logger2->debug("Bound assessment data to AptID [{$myAppointment->getAppointmentId()}].");

            // creation of the plan object must take place after binding of the Subjective object.
            // This is do to the isSpeech() dependancy on subjective data.
            $myPlan = $myAppointment->getPlan();
            if ( empty($myPlan)) {
                $myPlan = $this->createPlanFromAppointment($myAppointment, $myUser);
            }
            $myPlan->PatientID = $PatientID;

            if (! $myPlan->bind ( $postData )) {
                $error [] = new ErrorMessage ( "unable to bind Plan data" );
            }
            $logger2->debug("Bound plan data to AptID [{$myAppointment->getAppointmentId()}].");

            $myBillingSheet = $this->createBillingSheet($AptID, $PatientID, $mySession->CompanyID, NULL, $mySession->FacilityID, false, $myAppointment->getAptType());
            $myBillingSheet->PatientID = $PatientID;
            if (! $myBillingSheet->bind ( $postData )) {
                $error [] = new ErrorMessage ( "unable to bind Billing Sheet data<br/>" );
            }
            $logger2->debug("Bound billing sheet data to AptID [{$myAppointment->getAppointmentId()}].");

            //Apply the custom GP, GNP, GO modifier to the billing sheet if the POST variable is set.
            if ($request->isObjectiveApplyCptModifierSet())
            {
                $myBillingSheet->setCustomPrefix($request->getObjectiveApplyCptModifier());
            }

            $precautions = $this->getNewPatientPrecautionsInstance();
            $precautions->AptID = $AptID;
            $precautions->bind ( $postData );
            $precautions->store ();

            $dateOfService = $postData['DateYear'] . '-' . $postData['DateMonth'] . '-' . $postData['DateDay'];

            $arrErr = $mySubjective->check($dateOfService, $Medicare, $AptType, $request->getSubmitted(), $Pediatric, $Medicaid);
            $error = array_merge($arrErr, $error);

            if ($this->isSoapTestsPerformanceEnabled() && !$request->isAutoSave()) {
                $this->getSoapTestCacheCleanupService()->cleanup();
            }

            $mySubjective->store ( true );

            if (true == isset ( $mySubjective->SOAPID )) {
                /** @var T436 $t436Test */
                $t436Test = $this->buildTestFactory('T436');
                $t436Test = $t436Test->LoadTestBySOAPID ( $mySubjective->SOAPID );
                if (0 < count ( $t436Test )) {
                    $t436Test = $t436Test [0];
                }

                /** @var T436 $t436Test */
                $t436TestValues = $this->buildTestFactory('T436');
                $t436TestValues->SOAPID = $mySubjective->SOAPID;

                if ($t436Test->RecordID) {
                    $t436TestValues->RecordID = $t436Test->RecordID;
                    $t436TestValues->TMcGillQue = $mySubjective->McGillPainQuestionaire;

                    // 0 is a valid score; empty string != 0
                    $t436TestValues->TMcGillQueScore = 0 === strlen(trim($mySubjective->McGillPainScore)) ? NULL : ( int ) $mySubjective->McGillPainScore;
                    $t436TestValues->TMcGillQuePrintCheck = ( int ) $mySubjective->McGillQueCompleted;
                    $t436TestValues->TMcGillQuePrint = $mySubjective->McGillPainQuestionairePrint;
                    $t436TestValues->TMcGillQuePrintFollowUp = trim ( $mySubjective->PainFollowUpPlan );
                    $t436TestValues->TMcGillQuestions = trim ( $mySubjective->McGillQuestions );
                } else {
                    $t436TestValues->TMcGillQue = $mySubjective->McGillPainQuestionaire;
                    $t436TestValues->TMcGillQueScore = ( int ) $mySubjective->McGillPainScore;
                    $t436TestValues->TMcGillQuePrintCheck = $mySubjective->McGillQueCompleted;
                    $t436TestValues->TMcGillQuePrint = ( int ) $mySubjective->McGillPainQuestionairePrint;
                    $t436TestValues->TMcGillQuePrintFollowUp = trim ( $mySubjective->PainFollowUpPlan );
                    $t436TestValues->TMcGillQuestions = trim ( $mySubjective->McGillQuestions );
                }
                if (0 < $mySubjective->SOAPID) {
                    $t436TestValues->store ( true );
                }
                $logger2->debug("Subjective data for AptID [{$myAppointment->getAppointmentId()}] was persisted.");
            } else {
                $bt = debug_backtrace();
                $logger->error("subjective not created for for appointment " . $myAppointment->AptID . ". " . json_encode($bt));
            }

            if ($this->isIromsExclusionEnabled() && !is_null($mySubjective->getSoapId())) {
                $pqri = $this->createNewPqriInstance();
                $pqriProvider = $this->createNewPqriProviderInstance($pqri, $mySession->getFacilityID());
                $pqriProvider->setTargetPeriodDate($myAppointment->getDate());

                $soapId = $mySubjective->getSoapId();
                $iromsExclusionDto = new SoapIromsExclusionException();
                $iromsExclusionDto->setSoapId($soapId);

                if ($this->isValidIromsExclusionQuestion($mySubjective, $myUser, $pqriProvider)) {
                    $iromsExclusionDto->setIromsExclusionYn($mySubjective->getIromsExclusionYn());
                    $iromsExclusionDto->setIromsExclusionReasonId($mySubjective->getIromsExclusionReasonId());
                    $this->getIromsExclusionPersistenceService()->save($iromsExclusionDto);
                } else {
                    $iromsExclusionSoapId = $this->getIromsExclusionPersistenceService()->getBySoapId($soapId);

                    if ($iromsExclusionSoapId) {
                        $iromsExclusionDto->setIromsExclusionYn(null);
                        $iromsExclusionDto->setIromsExclusionReasonId(null);
                        $storedSoapIromsExclusion = $this->getIromsExclusionPersistenceService()->save($iromsExclusionDto);

                        if ($storedSoapIromsExclusion) {
                            $mySubjective->setIromsExclusionYn(null);
                            $mySubjective->setIromsExclusionReasonId(null);
                        }
                    }
                }
            }

            $myAppointment->bind ( $postData );
            $err = $myAppointment->check();
            $error = array_merge($err, $error);

            if (! $addn && ! $myAppointment->isDailyNoteGroup()) {
                if ($myAppointment->AptID > 0) {
                    $myAppointment->checkForDuplicate ( $error );
                }
            }

            $myAuthorization = $this->createNewAuthorizationWithDbInstance($objReadOnlyDatabase);
            $myAuthorization->loadActiveAuth ( $myPTCase );

            if ($myAuthorization->AuthorizationID) {
                $count = $myPTCase->countDN(array($myAppointment->CaseID), $myAuthorization->StartDate) + 1;
                if ($count >= $myAuthorization->Visits) {
                    $myAlert = $this->createNewDashAlertsInstance();
                    $myAlert->loadAnyWith ( 0, $patient->PatientID, 3, $patient->CaseID );
                    if (! ($myAlert->RecordID && $myAlert->AlertStatus != 'C')) {
                        $myAlert->PatientID = $patient->PatientID;
                        $myAlert->CaseID = $myAppointment->CaseID;
                        $myAlert->UserID = $patient->TherapistID;
                        $myAlert->AlertStatus = "A";
                        $myAlert->AlertTypeID = $myAlert::ALERT_TYPE_ID_AUTHORIZATION;
                        $myAlert->store ();
                    }
                }
            }

            $err = $myAssessment->check ( $Medicare, $Pediatric, $request->getSubmitted(), $Medicaid );
            $error = array_merge($err, $error);

            //Set GP to an array of arrays. GP[0] is an array of the GoalIDs, GP[1] is an array of the ProblemIDs. Returned in response data for auto-save. Regular save ignores this.
            $GP = $myAssessment->store();
            $logger2->debug("Assessment data for AptID [{$myAppointment->getAppointmentId()}] was persisted.");

            // Hot fix D-01272 (Duplicate Problems and Goals Non Med + FL)
            if ($request->isSubmittedPreview()) {
                $myAssessment->updatePostGoalsIds($request, $GP);
                $myAssessment->updatePostProblemsIds($request, $GP);
            }

            if($myAppointment->isProgressNote() && $request->isReCertification())
            {
                $myPlan->Recertification = 1;
            }else{
                $myPlan->Recertification = 2;
            }

            if ($request->isIncludeDocSign())
            {
                $myPlan->IncludeDocSign = 1;
            } else {
                $myPlan->IncludeDocSign = 2;
            }

            //Wipe re-certification if this is a billing tab attached to a PN re-certification
            if (($myAppointment->isDailyNote() || $myAppointment->isPlanOfCare()) && $myPlan->Recertification == 1)
            {
                $myPlan->Recertification = null;
            }

            $err = $myPlan->check($request);
            $error = array_merge($err, $error);

            if ( ! $request->isSubmittedDraft())
            {
                $expansion = $myPlan->getExpansion();

                if ($myAppointment->isOrthosisFabricationGroup() && $myPlan->hasRequestedDischarge())
                {
                    $path = $myAppointment->getPatient()->getAssetsPath('/docs');
                    $filename = $myAppointment->getDate(). '-' . $myAppointment->getPatientID() . '-' . $myAppointment->getCaseID() . '-DQ.pdf';
                    $finalize = $path . '/' . $filename;

                    if (true === empty($expansion)){
                        $error[] = new ErrorMessage('Discharge reason is required (See Plan Tab).');
                    } elseif (false === $myAppointment->hasAddendum() && true === $this->fileExists($finalize)) {
                        $error[] = new ErrorMessage('Discharge already exists for the date of service. Remove discharge option (See Plan Tab).');
                    }
                }
            }

            $myPlan->store();
            // verify plan was stored successfully
            if(!isset($myPlan->SOAPID)) {
                $bt = debug_backtrace();
                $logger->error("plan not created for for appointment " . $myAppointment->AptID . ". " . json_encode($bt));
            }
            $logger2->debug("Plan data for AptID [{$myAppointment->getAppointmentId()}] was persisted.");
            $previewForwardOrFinalized = ($request->isSubmittedPreview() || $request->isSubmittedForward() || $request->isSubmittedFinalize());
            $myBillingSheet->applyModifiers( $patient, $myUser, $myAppointment, $myClinic, $myPTCase, $postData, $previewForwardOrFinalized, $error);

            $pqri = $this->storePqriData(
                    $mySession, $myAppointment, $myUser, $patient, $myMods, $myClinic, $myBillingSheet,
                    $pqriAptID, $pqriAddn, $AptID, $AptType,
                    $request, $requestData, $error
                );
            $logger2->debug("PQRS data for AptID [{$myAppointment->getAppointmentId()}] was persisted.");


            $myBillingSheet->cleanupCptCodes();
            $myBillingSheet->store ( true );
            $logger2->debug("BillingSheet data for AptID [{$myAppointment->getAppointmentId()}] was persisted.");

            //Load fresh data for billing sheet
            $myBillingSheet = $this->createBillingSheet($AptID, $PatientID, $mySession->CompanyID, NULL, $mySession->FacilityID );

            foreach ( $error as $key => $value ) { // get rid of empty entries
                if (! $value) {
                    unset ( $error [$key] );
                }
            }

            //check if everything is correct in the billingSheet (check for duplicates CPT Codes) only if the action is preview, forward or Finalize Document, if the BILLING SHEET IS SELECTED or it's a daily note
            //This code must be after the store to make sure we have the latest changes before to make the check.
            if (($myAppointment->isDailyNoteGroup() || $myAppointment->includesDailyNote())
                && ($previewForwardOrFinalized))
            {
                $myBillingSheet->check($error, $dateOfService);
                $aptTypeSwitch = (in_array($myAppointment->getAptType(), array('DN', 'OD')))?true:false;
                $myBillingSheet->checkDirectTimedCPTCodes($error, $aptTypeSwitch);
                //Check if the valid amount of modifiers is being surpassed.
                if ($this->getServiceManager()->has(ModifiersLimitAmount::class)) {
                    /** @var ModifiersLimitAmountInterface $modifiersLimitAmount */
                    $modifiersLimitAmount = $this->getServiceManager()->get(ModifiersLimitAmount::class);
                    $error = $modifiersLimitAmount->checkModifiersLimitAmount($myBillingSheet, $error);
                }
            }

            if ($myAppointment->isDailyNoteGroup())
            {
                if ($this->isCachePayerQueryEnabled()) {
                    /** @var PayerByPiidDaoInterface $doa */
                    $doa = $this->getServiceManager()->get(PayerByPiidProxy::class);
                } else {
                    if ($this->isMovetoRoPayerQueryEnabled()) {
                        /** @var PayerByPiidDaoInterface $doa */
                        $doa = $this->getServiceManager()->get(PayerByPiidDao::class);
                    } else {
                        /** @var PayerDao $doa */
                        $doa = $this->getDaoFactoryInstance()->get('Wpt\Persistence\Payer\PayerDao');
                    }
                }
                $primaryPayerDto = $doa->getPayerByPiid($myPTCase->getPrimaryPolicy());
                $secondaryPayerDto = $doa->getPayerByPiid($myPTCase->getSecondaryPolicy());
                if($primaryPayerDto){
                    $myAppointment->setPrimaryInsTypeID($primaryPayerDto->getTypeID());
                }
                if($secondaryPayerDto){
                    $myAppointment->setSecondaryInsTypeID($secondaryPayerDto->getTypeID());
                }

                /*
                 * DE1986 - Check that if this is the DN Addendum, and it comes from an Addendum
                 * that if the Original Addendum has a CoSigner value, then save it into the attached Daily Note Addendum
                 */
                if($addn > 0 && $request->isValidOriginalAddendumId())
                {
                    $objOriginalAppointment = $this->createNewAppointmentInstance();
                    $objOriginalAppointment->load($request->getOriginalAppointmentId());
                    /*
                     * Check that the Original Addendum has a CoSigner value and has a Daily Note Included
                     */
                    if($objOriginalAppointment->getCosigner() != 0 && $objOriginalAppointment->getCosigner() != NULL && $objOriginalAppointment->getIncludeDailyNoteID() != 0){
                        $myAppointment->setCosigner($objOriginalAppointment->getCosigner());
                    }
                }

                $myAppointment->store (); //to at least save the date on the thing.

                if ($myPlan->Instructions == "DN3") {
                    $myAlert = new dashAlerts ();
                    $myAlert->loadAnyWith ( $patient->TherapistID, $patient->PatientID, 1, $patient->CaseID );
                    if (! $myAlert->RecordID) {
                        $myAlert->PatientID = $patient->PatientID;
                        $myAlert->CaseID = $myAppointment->CaseID;
                        $myAlert->UserID = $patient->TherapistID;
                        $myAlert->AlertStatus = "A";
                        $myAlert->AlertTypeID = $myAlert::ALERT_TYPE_ID_PROGRESS_NOTE;;
                        $myAlert->Removable = 1;
                        $myAlert->store ();
                    }
                }

            } else {
                $myAppointment->DraftArray = $request->getArrTests();
                //store does not update empty values, making it does so add unknown logic to the code this is a way to force an empty draft array
                if(empty($myAppointment->DraftArray)){
                    $myAppointment->storeEmptyDraftArray();
                }

                $myAppointment->store(false);
            }

            if (isset($_POST['submitted']) && $_POST['submitted'] == 'Finalize Document')
            {
                /** @var CaseHasAppointmentOnFacility $entity */
                $entity = CacheEntityManager::factory('Wpt\Cache\Entity\CaseHasAppointmentOnFacility', $myAppointment->CaseID);
                $entity->delete($entity::FORMAT_HAS_APTS_IN_FACILITY, array($myAppointment->Appntmnt_FacilityID));
            }

            $objErr = $myObjective->check ($Medicare, $AptType, $request->getSubmitted());
            $error = array_merge($objErr, $error);

            $myObjective->store ();
            $logger2->debug("Objective data for AptID [{$myAppointment->getAppointmentId()}] was persisted.");

            //	Set case to active if it is discharged.
            if ( ! $myPTCase->isActive() && ! $request->isSubmittedDraft()
                && ($myAppointment->isExamGroup() || $myAppointment->isOrthosisFabricationGroup()))
            {
                $myPTCase->activate();
            }

            /**
             * Add/Remove Progress Note Alerts
             * Note: this needs to be executed after Evaluative Note or Daily Note is created, and just for Medicare
             */
            $myAppointment->checkPNneedAlert();

            //If the user does not have finish soap note privileges and forwards the note, this will set the fields for forwarding which in turn creates an alert on the dashboard
            //for the PT to whom the note was forwarded.
            if ($request->isSubmittedForward())
            {
                if ($request->hasForwardId())
                {
                    if ($request->isSpecialDn())
                    {
                        $sAptType = $myAppointment->AptType;
                        $patient->validateNote ( $error, $AptType, $myUser, $myCP, $myMods, $myAppointment, $mySubjective, $myObjective, $myAssessment, $myPlan, $myBillingSheet,$myPTCase );

                        $e = $myAppointment->getDailyNoteType();
                        $myAppointment->AptType = $e;
                        $patient->validateNote ( $error, $e, $myUser, $myCP, $myMods, $myAppointment, $mySubjective, $myObjective, $myAssessment, $myPlan, $myBillingSheet,$myPTCase );

                        $myAppointment->AptType = $sAptType;
                    } else {
                        $patient->validateNote ( $error, $AptType, $myUser, $myCP, $myMods, $myAppointment, $mySubjective, $myObjective, $myAssessment, $myPlan, $myBillingSheet, $myPTCase );
                    }

                    if (count($error) == 0) {
                        $myAppointment->CoSigner = $mySession->UserID;
                        $myAppointment->ForwardToPT = (int)$request->getForwardId();
                        $myAppointment->DateForwarded = gmdate ( 'Y-m-d H:i:s' );
                        $myAppointment->store ();

                        //	REMOVE CACHE GROUP: DASHBOARD_ATAGLANCE
                        $strDashboardAtGlanceGroup = 'DASHBOARD_ATAGLANCE_' . $mySession->FacilityID;
                        $this->getWptCacheInstance()->clearCache ( null, $strDashboardAtGlanceGroup );

                        if ( ! $request->isSpecialDn())
                        {
                            $this->wptRedirect ( "patientChart.php?ID=$PatientID&msg=4" );
                        }
                        else {
                            $return = array ();
                            $return ['error'] = 1;
                            $return ['AptType'] = $AptType;
                            $return ['AptID'] = $myAppointment->AptID;
                            $return ['Addn'] = $addn;
                            $return ['submitted'] = "Forward";
                            $return ['patientid'] = $PatientID;
                            $return ['msg'] = 4;
                            echo json_encode ( $return );
                        }
                        return;
                    }
                }
            }

            if ($request->hasSubmitted())
            {
	            /*
	             * Check for NC001 CPT Code and clear the cache
	             * to affect the Visits Until Evaluative Note Countdown
	             */
	            if ($myPTCase->isMedicare()) {
		            // Delete Namespace to clear cache
		            PNCountdown::getInstance()->getCacheEntityManagerFromCase($myPTCase)->deleteNamespace();
	            }

                if ( ! $request->isSubmittedDraft() && ! $request->isSubmittedForward())
                {
                    if ($request->isSpecialDn())
                    {
                        $sAptType = $myAppointment->AptType;
                        $patient->validateNote ( $error, $AptType, $myUser, $myCP, $myMods, $myAppointment, $mySubjective, $myObjective, $myAssessment, $myPlan, $myBillingSheet, $myPTCase );

                        $e = $myAppointment->getDailyNoteType();
                        $myAppointment->AptType = $e;
                        $patient->validateNote ( $error, $e, $myUser, $myCP, $myMods, $myAppointment, $mySubjective, $myObjective, $myAssessment, $myPlan, $myBillingSheet, $myPTCase );

                        $myAppointment->AptType = $sAptType;
                    } else {
                        $patient->validateNote ( $error, $AptType, $myUser, $myCP, $myMods, $myAppointment, $mySubjective, $myObjective, $myAssessment, $myPlan, $myBillingSheet, $myPTCase );
                    }

                    /*
                     * If this is a discharge that is being finalized then we need to check to make sure there are
                     * no notes in the patient chart that are not finalized.
                     */
                    if ( $myAppointment->isDischarge() || $myPlan->hasRequestedDischarge()) {
                        if ( ! $myPTCase->canDischarge( $myAppointment)) {
                            $error[] = new ErrorMessage(PTCase::ERROR_INCOMPLETE_NOTES_PREVENT_DISCHARGE);
                        }

                    }
                }

                // FUTURE DATE CHECK
                if ($request->isSubmittedFinalize() || $request->isSubmittedPreview())
                {
                    try {
                        $myAppointment->checkDateOfService();
                    } catch (FutureDate $e) {
                        $logger->info($e);
                        $message = "You cannot " . ($request->isSubmittedFinalize() ? 'finalize':'preview')
                            . " this note because the date of service is in the future. See the Subjective tab to correct.";
                        // log error
                        $error[] = new ErrorMessage($message);
                    } catch (NullDate $e) {
                        $logger->info($e);
                        $error[] = new ErrorMessage("Date of Service is a required field");
                    } catch (Exception $e) {
                        $logger->info($e);
                        $error[] = new ErrorMessage($e->getMessage());
                    }
                    $contextDto = new LegacyHarnessContextDto();
                    $contextDto->setAppointment($myAppointment);
                    $contextDto->setRequest($request);
                    $contextDto->setAppointmentId($AptID);

                    if($this->getFeatureFlip()->enabled('enr-b24601-validation-to-allow-only-one-billing-unit')) {
                        $contextDto->setBillingSheet($myBillingSheet);
                    }

                    $contextDto->setRequestData($requestData);
                    $soapValidationErrorService = $this->getSoapValidationErrorService();
                    $soapValidationErrorService->setContext($contextDto);
                    $soapValidationErrors = $soapValidationErrorService->getErrors();
                    foreach ($soapValidationErrors as $soapValidationError) {
                        array_push($error, $soapValidationError);
                    }

                    if($this->getServiceManager()->has(SoapValidatorInterface::class)){
                        /** @var SoapValidatorInterface $preSoapSaveValidator */
                        $preSoapSaveValidator = $this->getServiceManager()->get(SoapValidatorInterface::class);
                        if(!$preSoapSaveValidator->isValid($contextDto)){
                            $messages = $preSoapSaveValidator->getMessages();
                            foreach ($messages as $errorMessage) {
                                $error [] = new ErrorMessage($errorMessage) ;
                            }
                        }
                    }

                    if ($AptType === Appointment::TYPE_INITIAL_EXAM) {
                        $pqri = $this->createNewPqriInstance();
                        $pqriProvider = $this->createNewPqriProviderInstance($pqri, $mySession->getFacilityID());
                        $pqriProvider->setTargetPeriodDate($myAppointment->getDate());

                        $subjectiveSurgeryDto = new SubjectiveSurgeryContextDto();
                        $subjectiveSurgeryDto->setAppointmentType($AptType);
                        $subjectiveSurgeryDto->setPQRIProvider($pqriProvider);
                        $subjectiveSurgeryDto->setSubjectiveDateInitialExamination($mySubjective->getDateOfService());
                        $subjectiveSurgeryDto->setSurgeryPerform($mySubjective->getSurgery());
                        $subjectiveSurgeryDto->setSurgeryDate($mySubjective->getSurgeryDate());
                        $subjectiveSurgeryDto->setUserTypeId($myUser->getUserTypeId());
                        $subjectiveSurgeryDto->setUser($myUser);

                        $soapFinalizationSurgeryValidator = $this->getSoapFinalizationSurgeryValidator();
                        if (!$soapFinalizationSurgeryValidator->isValid($subjectiveSurgeryDto)) {
                            foreach ($soapFinalizationSurgeryValidator->getMessages() as $errorMessage) {
                                $error [] = new ErrorMessage($errorMessage);
                            }
                        }

                        if ($this->isIromsExclusionEnabled() &&
                            $this->isValidIromsExclusionQuestion($mySubjective, $myUser, $pqriProvider) &&
                            !$this->getIromsExclusionRequiredValidator()->isValid($myAppointment)) {
                            foreach ($this->getIromsExclusionRequiredValidator()->getMessages() as $errorMessage) {
                                array_unshift($error, new ErrorMessage($errorMessage));
                            }
                        }
                    }
                }

                //If there are errors, this loops through all the errors that are not warnings and throws them into a string which is then
                //sent back to AddSoap to be displayed to the user.
                $errString = '';
                $htmlErrors = '';
                $arrTabs = array();
                if (count ( $error ))
                {
                    $myAppointment->load ( $AptID );
                    foreach ( $error as $e )
                    {
                        if (! $e->isWarning ())
                        {
                            if($e instanceof IsHtmlErrorMessage)
                            {
                                $htmlErrors .= "\n".$e->getMessage();
                            }
                            else
                            {
                                $errString .= "\n".$e->getMessage();
                                if ($errString == '' && $e instanceof ErrorMessageTab)
                                {
                                    $arrTabs[] = $e->getTab();
                                }
                            }
                        }
                    }
                    $htmlErrors = ltrim($htmlErrors, "\n");
                    $errString  = ltrim($errString, "\n");


                }
                if ($errString || $htmlErrors)
                {
                    /** @var SoapUrlProvider $urlProvider */
                    $urlProvider = StaticServiceManager::getInstance()->get('SoapUrlProvider');
                    $baseUrl = $urlProvider->getUrl();
                    if ($request->isSubmittedPreview())
                    {
                        /*
                         * Build the query string
                         */
                        $queryString = $this->buildRedirectQueryStringWithError($PatientID, $AptType, $myAppointment->AptID, $errString, $arrTabs, $error, $htmlErrors);

                        $strUrl = $baseUrl."?" . $queryString;
                        $this->wptRedirect ( $strUrl );
                    }

                    if ( ! $request->isSpecialDn())
                    {
                        /*
                         * Build the query string
                         */
                        $queryString = $this->buildRedirectQueryStringWithError($PatientID, $AptType, $myAppointment->AptID, $errString, $arrTabs, $error, $htmlErrors);

                        $strUrl = $baseUrl."?" . $queryString;
                        $this->wptRedirect ( $strUrl );
                    }
                    else
                    {
                        $logger2->debug("Errors detected for AptID [{$myAppointment->getAppointmentId()}]: " . $errString);

                        $return = array ();
                        $return ['error'] = $errString;
                        $return ['htmlErrors'] = $htmlErrors;
                        $return ['AptType'] = $AptType;
                        $return ['AptID'] = $myAppointment->AptID;
                        $return ['Addn'] = $addn;
                        $return ['Goals'] = $GP[0];
                        $return ['Problems'] = $GP[1];
                        $return ['Tabs'] = $arrTabs;
                        $return ['ChangeTab'] = (count($error) == 1) ? true : false;
                        echo json_encode ( $return );
                    }
                    return;
                }

                /**
                 * Since we are applying the new logic for CO/CQ modifier
                 * when the payer doesn't have set Rule of 8s
                 * is possible that the user enter units different that the algorithm calculate
                 * in that case we need to override the unit with the calculated value
                 * in order to shown on the DPF and send on the Snapshot
                 */
                if ($this->featureFlip->enabled(FeatureFlipConstant::ASSISTANT_THERAPIST_MINUTES)) {
                    $coCqBillingService = $this->getCoCqBillingService();
                    $coCqBillingService->updateCoCqUnits($myBillingSheet, $myAppointment);
                }
                /*
                 * Clear the appointment discharged method cache here.  This need to happen
                 * on save because dates on a note can change which can make the cache invalid.
                 */
                if ( ! $myAppointment->isMissedAppointmentGroup() && ! $myAppointment->isDischargeSummary())
                {
                    $this->getWptCache2Instance()->delete('APPOINTMENT_DISCHARGED_' . $patient->PatientID . '_' . $patient->CaseID);
                }

                if ($request->isSubmittedDraft())
                {
                    //If it is a save as draft then the note is already saved and the user is redirected back to the Patient chart.
                    $strUrl = "patientChart.php?ID=" . $PatientID;
                    $this->wptRedirect ( $strUrl );
                    return;
                }

                if ($request->isSubmittedPreview()) {
                    // At this point, preview has saved everything it needs to.
                    return;
                }

                if ($this->isRemoveInsuranceRefPhysicianEnabled() && $request->isSubmittedFinalize()) {
                    $appointmentHasPayerService = $this->getAppointmentHasPayerService();
                    $appointmentHasPayerService->save($myAppointment, $myPTCase, $mySession->getCompanyID());
                }

                if ( ! $request->isSpecialDn()
                    || ($request->isSubmittedSet() && ! $booIncludeDailyNote && $addn > 0))
                {
                    $params = new PreviewFinalizeDto;
                    $params->setAppointment($myAppointment);
                    $params->setAptID($AptID);
                    $params->setAuthenticatedUser($myUser);
                    $params->setCase($myPTCase);
                    $params->setClinicPreferences($myCP);
                    $params->setLogger($logger);
                    $params->setSession($mySession);

                    // Get last appointment before note is finalized.
                    $lastAppointmentObj = $myAppointment->getOriginalNote();

                    $finalizeService = new PreviewFinalizeService;
                    $msg = $finalizeService->finalize($params);

                    $this->attachPostSaveEventListener();
                    $this->triggerPostSaveEvent($request);

                    // Process WebOutcomes Visit/Questionnaire and Create WebPT WO Document
                    $this->processWebOutcomesVisit($myAppointment, $lastAppointmentObj, $requestData);

                    /*
                     * In this part all data from appointments is store in this variable $myAppointment->appointments->_Document
                     * Here is the data required
                     */
                    if ($this->isAuditLogEventSentCreateSoapEnabled()) {
                        $this->emitNewDocument($myAppointment);
                    }

                    if ($this->isEmrServiceDpiDataForPatientEnabled()) {
                        $this->getDpiUpdater()->process([$myAppointment]);
                    }

                    if ($request->isAjax())
                    {
                        echo json_encode(array(
                            'error' => $msg,
                            'AptType' => $AptType,
                            'AptID' => $myAppointment->AptID,
                            'Addn' => $addn,
                            'Goals' => $GP[0],
                            'Problems' => $GP[1],
                        ));

                        return;
                    }

                    // PDF was created. Redirect to patient chart.
                    $this->wptRedirect('patientChart.php?' . http_build_query(array(
                        'ID' => $myAppointment->getPatientID(),
                        'msg' => $msg,
                    )));

                    return;
                }

                //Update link between billing sheet and original note
                if ( 0 < ($intOriginalAptID = $postData['originalAptID']) && $myAppointment->isDailyNoteGroup()) {
                    $objAppointment = new Appointment();
                    $objAppointment->load($intOriginalAptID);
                    if(true === isset($objAppointment->AptID)) {
                        $objAppointment->setIncludeDailyNoteID($myAppointment->AptID);
                        $objAppointment->store();
                    }
                }
                //Check to make sure that there is a POC that needs to get created and that it's not a daily note.
                if ( ! $myAppointment->isDailyNoteGroup() && 1 == $myPlan->CreatePOC)
                {
                    $this->createSoapPlanOfCare($objReadOnlyDatabase, $myAppointment, $mySubjective, $myAssessment, $myPlan, $mySession, $myCP);
                }

                // Get last appointment before note is finalized.
                $lastAppointmentObj = $myAppointment->getOriginalNote();

                $msg = $this->createPDF($myAppointment, $AptID, true, $intOriginalAptID, $myCP);
                //log for FLRGcode issue
                $this->hasErrorOnFlrGcode();

                /*
                 * Initial examination - Progress Note - Re-Examine
                 */
                if ($this->isAuditLogEventSentCreateSoapEnabled()) {
                    $this->emitNewDocument($myAppointment);
                }

                if ($this->isEmrServiceDpiDataForPatientEnabled()) {
                    $this->getDpiUpdater()->process([$myAppointment]);
                }

                // Process WebOutcomes Visit/Questionnaire and Create WebPT WO Document
                $this->processWebOutcomesVisit($myAppointment, $lastAppointmentObj, $requestData);

                //US3774: need to save the Progress Note needed alert
                if ($request->isSubmittedFinalize())
                {
                    $myAppointment->checkPNneedAlert();

                    //US6277 If this is an addendum
                    if ($myAppointment->getAppointmentID()<0){
                        //Check whether or not FLR has changed
                        if( $myPTCase->isMedicare())
                        {
                            $flrService = FlrMedicareService::getInstance();
                            if($flrService->isFLDischarge($myAppointment)
                                && ($flrService->isPrimaryFLChangedForAddendum($myAppointment) ||
                                    $flrService->isFLModifiersChanged($myAppointment)  )
                            )
                            {
                                //Primary FLR has changed for Addendum from Original Appointment
                                $nextAppointment = $flrService->getNextFinalizedNoteWithDateOfServiceAfter($myAppointment);
                                if($nextAppointment){
                                    $addendumRequiredService = new AddendumRequiredService();
                                    $addendumRequiredService->addMedicareFlrAddendumRequired(
                                        $nextAppointment->getCaseId(),
                                        $nextAppointment->getAppointmentId()
                                    );
                                }
                            }
                        }
                    }

                    if($myAppointment->isEvaluativeGroup()){
                        $this->saveAppointmentDiagnosis($myAppointment, $myPTCase);
                    }
                }

                /**
                 * Check to see if visit will expire or cross alert threshold for any current prescriptions
                 */
                if ($myAppointment->isDailyNoteGroup())
                {
                    $this->getWptPatientPrescriptionInstance()->updateExpiringByVisit($myAppointment->getCaseID(), $myAppointment->getDate());
                }

                /**
                 * If medicare patient addendum, delete all prior PocSignatureStatus records to reset POC that a request for signature needs to be re-initiated
                 **/
                if($patient->isMedicare())
                {
                    $this->planOfCareStatusUpdateForAddendum( $myAppointment );
                }

                /** @var FeatureFlipInterface $featureFlip */
                $featureFlip = $this->getServiceManager()->get('FeatureFlip');

                $qdOneTimeVisitEnabled = $featureFlip->enabled('one-time-visit-quick-discharge');

                /** @var QuickDischargePdfCreatorInterface $quickDischargePdfCreator */
                $quickDischargePdfCreator = $this->getServiceManager()->get(QuickDischargePdfCreatorInterface::class);

                /* If an IE with One Time Visit is selected - see if we need to discharge pdf - actually making the case discharged happens somewhere else */
                if ($qdOneTimeVisitEnabled && $this->canCreateQuickDischargePdf($msg, $myAppointment, 'DN') && $mySubjective->isOneTimeVisit()) {
                    $quickDischargePdfCreator->createQuickDischargePdf($mySession, $myAppointment, $myPTCase, $mySubjective, $myPlan, 'Discharged via one time visit.');
                }

                /* If an OD used with Billing Tab - see if we need to discharge pdf - actually making the case discharged happens somewhere else */
                if ($this->canCreateQuickDischargePdf($msg, $myAppointment, 'OD') && $myPlan->hasRequestedDischarge()) {
                    $quickDischargePdfCreator->createQuickDischargePdf($mySession, $myAppointment, $myPTCase, $mySubjective, $myPlan);
                }

                //  Discharge case if is DS notes set it by the plan or if it's a One Time Visit Note
                if (($myPlan->hasRequestedDischarge() || $mySubjective->isOneTimeVisit()) && !$isAutoSave) {

                    //  Do so only if note has no included daily note, or if it does and this save is the included daily note.
                    if (!$myAppointment->includesDailyNote() || ($myAppointment->isDailyNoteGroup() && 0 < $intOriginalAptID)) {

                        if ($myPTCase->canDischarge($myAppointment)) {
                            $myPTCase->discharge();
                        } else {
                            throw new SaveSoapException(array('error' => PTCase::ERROR_INCOMPLETE_NOTES_PREVENT_DISCHARGE),
                                "patientChart.php?ID={$myAppointment->PatientID}&CaseID={$myAppointment->CaseID}&msg=19");
                        }
                    }
                }

                try {
                    /*
                     * Evaluate if this is a daily note for billing service eligibility
                     * Create the financial snapshot for daily notes.  Check the return value from the createPDF function
                     * if it was NULL or 1 then the PDF was created and doc ID saved so the snapshot can be created.
                     */
                    $isRebillRequest = $request->isReBill() || $request->isSubmittedReBill();
                    if (($msg === NULL || $msg === 1)
                        && (($myAppointment->isDailyNoteGroup() && 0 < $AptID)
                            || ($myAppointment->isDailyNoteGroup() && 0 > $AptID && $isRebillRequest)))
                    {
                        if ($AptID < 0) {
                            /** @var $addendumDao AddendumDao */
                            $addendumDao = $this->getDaoFactoryInstance()->get('Wpt\Persistence\Appointment\AddendumDao');
                            $addendumDao->setRebill(-$AptID, 1);
                        }

                        $this->getSnapshotService()->process($contextDto);

                    } else {
                        if (isset($myAddendum)) {
                            /** @var $addendumDao AddendumDao */
                            $addendumDao = $this->getDaoFactoryInstance()->get('Wpt\Persistence\Appointment\AddendumDao');
                            $addendumDao->setRebill(-$AptID, 0);
                        }
                    }
                } catch (Exception $objException) {
                    $logger->error('Snapshot creation has failed: ' . $objException->getMessage() . "File: {$objException->getFile()}  \nLine: {$objException->getLine()} \nTrace: {$objException->getTraceAsString()}", $objException);
                }

                $this->attachPostSaveEventListener();
                $this->triggerPostSaveEvent($request);

                $return = array();
                $return['error'] = $msg;
                $return['AptType'] = $AptType;
                $return['AptID'] = $myAppointment->AptID;
                $return['Addn'] = $addn;
                $return ['Goals'] = $GP[0];
                $return ['Problems'] = $GP[1];
                echo json_encode($return);

                return;
            }

            if ( $isAutoSave) {
                header ( 'Content-type: application/json' );
                if ($postData) {
                    $return = array ();
                    $return ['date'] = getdate ();
                    $return ['AptID'] = $myAppointment->AptID;
                    $return ['Addn'] = $addn;
                    $return ['Goals'] = $GP [0];
                    $return ['Problems'] = $GP [1];
                    $arrReturnTests = array ();
                    foreach ( $myObjective->getTests() as $objTest ) {
                        $arrReturnTests [$objTest->TestID] = $objTest->RecordID;
                    }
                    foreach ( $mySubjective->arrTests as $objTest ) {
                        $arrReturnTests [$objTest->TestID] = $objTest->RecordID;
                    }
                    $return ['TestIDs'] = $arrReturnTests;
                } else {
                    $return = "Post not received.";
                }
                echo json_encode($return);
                return;
            }
        }catch (SaveSoapException $e){
            // log that we handled save soap exception
            $logger->error('SaveSoapException: ' . $e->getReturnAsString());
            // log that we got far enough to create an appointment stub but not subjective or plan
            if(isset($myAppointment) && isset($myAppointment->AptID)) {
                // appointment missing subjective
                if(isset($mySubjective) && !isset($mySubjective->SOAPID)) {
                    $logger->error('Experienced SaveSoapException before subjective was created for Appointment ' .$myAppointment->AptID . '.');
                }
                // appointment missing plan
                if(isset($myPlan) && !isset($myPlan->SOAPID)) {
                    $logger->error('Experienced SaveSoapException before plan was created for Appointment ' .$myAppointment->AptID . '.');
                }
            }

            if ( $isAutoSave || 'XMLHttpRequest' === $_SERVER['HTTP_X_REQUESTED_WITH']){
                header ( 'Content-type: application/json' );
                echo json_encode(array_merge(array(
                        'patientID' => $PatientID,
                        'caseID' => $CaseID,
                    ),
                    $e->getReturn()
                ));
                //exit;
            }else{
                $this->wptRedirect($e->getUrl());
            }

        } catch (SnapshotException $e){
            $logger->error('SnapshotException: ' . $e->getMessage());
            // we only caught exception to provide a very specific logging hook, rethrow to let some other process actually handle it
            throw $e;
        } catch (Exception $e) {
            $loggedMsg = '';
            // log that we got far enough to create an appointment stub but not subjective or plan
            if(isset($myAppointment) && isset($myAppointment->AptID)) {
                // See if any useful information was being held in the $err variable before exception
                $errDump = isset($err) ? ': '.$err : '';

                // appointment missing subjective
                if(isset($mySubjective) && !isset($mySubjective->SOAPID)) {
                    $loggedMsg ='Experienced Exception before subjective was created for Appointment. ' .$myAppointment->AptID . '. ';
                }
                // appointment missing plan
                if(isset($myPlan) && !isset($myPlan->SOAPID)) {
                    if ( !isset($pqri) ){
                        $loggedMsg ='Experienced Exception before PQRI instantiated. ' . $errDump . $myAppointment->AptID . '. ';
                    } elseif (!isset($myAuthorization)){
                        $loggedMsg ='Experienced Exception before Authorizartion instantiated. ' . $errDump . $myAppointment->AptID . '. ';
                    } elseif (!isset($GP)){
                        $loggedMsg ='Experienced Exception before Assessment instantiated. ' . $errDump . $myAppointment->AptID . '. ';
                    } else {
                        $loggedMsg ='Experienced Exception before plan was created for Appointment. ' . $errDump . $myAppointment->AptID . '. ';
                    }
                }
            }
            if($loggedMsg == ''){
                $appointmentID = isset($myAppointment->AptID)? $myAppointment->AptID : ' AppointmentID not set';
                $loggedMsg = 'Experienced Exception while saving soap note '. $errDump . $appointmentID . '. ';
            }
            $logger->error($loggedMsg . $e->getMessage() . "\n" . $e->getTraceAsString());
            // we only caught exception to provide a very specific logging hook, rethrow to let some other process actually handle it
            throw $e;
        }

    }

    /**
     * @param int $PatientID
     * @param string $AptType
     * @param int $AptID
     * @param string $errString
     * @param array $arrTabs
     * @param array $error
     * @param string $htmlError
     * @return string
     */
    protected function buildRedirectQueryStringWithError($PatientID, $AptType, $AptID, $errString, $arrTabs, $error, $htmlError)
    {
        /*
         * Build an array with the parameters needed, plus the error tabs when the ErrorMessageTab object
         * is used
         */
        $arrQueryString = array(
            'ID'        => $PatientID,
            'AptType'   => $AptType,
            'myAptID'   => $AptID,
            'error'     => $errString,
            'errorTabs' => $arrTabs,
            'changeTab' => (count($error) == 1) ? 1 : 0,
            'htmlErrors' => $htmlError,

        );

        /*
         * Build the query string from the array
         */
        return http_build_query($arrQueryString);
    }

    /**
     * 
     * @param Therapist $myUser
     * @param PQRIProvider $pqriProvider
     * @param \Appointment $myAppointment
     * @param wptSession $mySession
     * @param bool $Medicare
     * @return bool
     */
    protected function canStorePqriData($myUser, $pqriProvider, $myAppointment, $mySession, $Medicare)
    {
        //Validate if MIPS is needed
        $mipsData = new MipsData();
        $mipsData->setUser($myUser);

        $isValid = $this->isValidationMet($mipsData);
        if ($isValid === false) {
            return $isValid;
        }

        $backPainService = BackPainService::getInstance();
        $isBackPainValid = $backPainService->isBackPainValid($myUser, $pqriProvider);
        // additional check for backpain, since it disables PQRS after 20 patients
        $backPainValidated = !( $isBackPainValid &&
            $backPainService->isBackPainTherapistPatientLimitMet($myUser, $myAppointment, $mySession->getFacilityID()));

        return $myAppointment->isPQRSNoteType() && $backPainValidated;
    }
    
    /**
     * Check if we are finalizing a note with back pain or
     * finalizing an addendum of a back pain valid note
     * @param array $requestData
     * @param bool $pqriAddn
     * @return bool
     */
    protected function isFinalizingBackPainNote($requestData, $pqriAddn)
    {
        $backPainService = BackPainService::getInstance();
        
        return isset($_POST['submitted']) && $_POST['submitted'] == 'Finalize Document' &&
                ($backPainService->isAppointmentRequestWithBackPainData($requestData) || $pqriAddn);
    }

    /**
     * Clear cached therapist back pain patients count
     * @param PQRIProvider $pqriProvider
     * @param Therapist $myUser
     * @param Clinic $myClinic
     * @param Appointment $myAppointment
     */
    protected function clearBackPainCountCache($pqriProvider, $myUser, $myClinic, $myAppointment)
    {
        $backPainService = BackPainService::getInstance();
        $currentPeriod = $pqriProvider->getCurrentPeriod();
        if(!empty($currentPeriod))
        {
            $periodId = $pqriProvider->getCurrentPeriod()->getPeriodId();
        }
        else
        {
            $currentPeriods = DaoFactory::getInstance()->get('Wpt\Persistence\PQRS\PeriodDao')->getByRegistryIdDate( $pqriProvider->getRegistryId(), $myAppointment->getDate() );
            /** @var FirstPqriPeriodInterface $firstPeriodService */
            $firstPeriodService = $this->getServiceManager()->get('Pqrs.FirstPqriPeriod');
            $periodId = $firstPeriodService->getFirstPqriPeriod($currentPeriods);
        }
        
        $backPainService->clearCachedTherapistBackPainPatientCount($myUser->getID(), $myClinic->getId(), $periodId);
    }

    /**
     * @return int
     */
    public function getPqrsCacheTtl()
    {
        if (is_null($this->pqrsCacheTtl))
        {
            $value = $this->getConfig()->getValue('performance_settings/Wpt/SOAP/Save/PQRS/cache/ttl', 7200);
            $this->setPqrsCacheTtl($value);
        }

        return $this->pqrsCacheTtl;
    }

    /**
     * @param int $ttl
     * @return self
     */
    public function setPqrsCacheTtl($ttl)
    {
        $this->pqrsCacheTtl = $ttl;
        return $this;
    }

    /**
     * @return int
     */
    public function getPqrsCacheEnabled()
    {
        if (is_null($this->pqrsCacheEnabled))
        {
            $value = $this->getConfig()->getValue('performance_settings/Wpt/SOAP/Save/PQRS/cache/enable', 1);
            $this->setPqrsCacheEnabled($value);
        }

        return $this->pqrsCacheEnabled;
    }

    /**
     * @param int $enabled
     * @return self
     */
    public function setPqrsCacheEnabled($enabled)
    {
        $this->pqrsCacheEnabled = $enabled;
        return $this;
    }

    /**
     * @param Appointment $appointment
     * @return AppointmentEntity|LegacyAppointmentCacheEntity
     */
    protected function getAppointmentCacheEntity(Appointment $appointment)
    {
        $cacheEntityClass = $this->getConfig()->getValue(
            'performance_settings/Wpt/SOAP/Save/PQRS/cache/entity',
            'Wpt\Cache\Entity\Legacy\Appointment'
        );

        return $this->getCacheEntityManager()->createEntity($cacheEntityClass, $appointment->getAppointmentId());
    }

    /**
     * @param Appointment $appointment
     * @return bool
     */
    protected function getPqrsChecksum(Appointment $appointment)
    {
        $entity = $this->getAppointmentCacheEntity($appointment);

        if($entity instanceof AppointmentEntity)
        {
            $result = $entity->get($entity::FORMAT_SAVE_SOAP_PERSIST, array(
                '_PQRS_' . $appointment->getAppointmentId(),
            ));
        }
        else
        {
            $result = $entity->get($entity::FORMAT_SAVE_SOAP_PERSIST, array(
                $this->getSession()->getId(),
                '_PQRS_' . $appointment->getAppointmentId(),
            ));
        }

        return $result;
    }

    /**
     * @param Appointment $appointment
     * @param string $checksum
     */
    protected function setPqrsChecksum(Appointment $appointment, $checksum)
    {
        $entity = $this->getAppointmentCacheEntity($appointment);

        if($entity instanceof AppointmentEntity)
        {
            $entity->set($checksum, $entity::FORMAT_SAVE_SOAP_PERSIST, array(
                '_PQRS_' . $appointment->getAppointmentId(),
            ), $this->getPqrsCacheTtl());
        }
        else
        {
            $entity->set($checksum, $entity::FORMAT_SAVE_SOAP_PERSIST, array(
                $this->getSession()->getId(),
                '_PQRS_' . $appointment->getAppointmentId(),
            ), $this->getPqrsCacheTtl());
        }
    }

    /**
     * @param Request $request
     * @param Appointment $appointment
     * @return bool
     */
    protected function shouldStorePqrsBasedOnChecksum(Request $request, Appointment $appointment)
    {
        if ( ! $this->getPqrsCacheEnabled())
        {
            return true;
        }

        // Get request PQRS answers and build a checksum to compare.
        $answers = $request->getPqrsAnswersWithText();
        ksort($answers);

        $checksum = md5(serialize($answers));

        // Compare request checksum against appointment cache.
        if ($checksum == $this->getPqrsChecksum($appointment))
        {
            // The request checksum was found for this appointment. Bail out.
            return false;
        }

        // The checksum is new. Store it and continue.
        $this->setPqrsChecksum($appointment, $checksum);
        return true;
    }

    /**
     * @param wptSession $mySession
     * @param \Appointment $myAppointment
     * @param Therapist $myUser
     * @param Patient $patient
     * @param \ClinicModule $myMods
     * @param \Clinic $myClinic
     * @param \BillingSheet $myBillingSheet
     * @param int $pqriAptID
     * @param bool $pqriAddn
     * @param int $AptID
     * @param string $AptType
     * @param Request $request
     * @param array $requestData
     * @param mixed $error
     * @return PQRI
     */
    protected function storePqriData($mySession, $myAppointment, $myUser, $patient, $myMods, $myClinic, $myBillingSheet,
            $pqriAptID, $pqriAddn, $AptID, $AptType,
            Request $request, $requestData, &$error)
    {
        $isMedicare = $patient->isMedicare();
        $pqri = $this->createNewPqriInstance();
        $pqriProvider = $this->createNewPqriProviderInstance($pqri, $mySession->getFacilityID());
        $pqriProvider->setTargetPeriodDate($myAppointment->getDate());
        $recordableDto = $this->newRecordablePqrsDataDto(
            $requestData,
            $patient,
            $pqriAptID,
            $pqriAddn,
            $myAppointment->getDate(),
            $myClinic,
            $myUser,
            $myBillingSheet
        );

        // Has this pqrs request been previously checked successfully. If so, save system resources by not rechecking and continue to 
        // use the previously saved data
        if ( ! $this->shouldStorePqrsBasedOnChecksum($request, $myAppointment))
        {
            //D-01298 : We need to verify if the measures were answered to fill error var
            if ($myMods->PQRI && ! $request->isSubmittedDraft() && $this->canStorePqriData($myUser, $pqriProvider, $myAppointment, $mySession, $isMedicare))
            {
                // If an exception is thrown here, don't change anything
                $pqriProvider->check (
                    $requestData,
                    $error,
                    $patient,
                    $AptType,
                    $pqriAptID,
                    $myAppointment->Date,
                    $pqriAddn,
                    $myClinic,
                    $myUser,
                    $myBillingSheet,
                    $AptID,
                    $request->getPqrsAnswers()
                );
            }

            // D-01294 : bind PQRS G-codes to billng sheet so they are stored correctly later on
            if($pqriProvider->hydrateUpdateLists($recordableDto) === true)
            {
                $pqriProvider->bindBillingPqrsGcodes($recordableDto);
            }

            if ($this->hasValidHMMeasures($recordableDto)) {
                $this->setPqrsChecksum($myAppointment, '');
            }

            return $pqri;
        }


        if ( ! $this->canStorePqriData($myUser, $pqriProvider, $myAppointment, $mySession, $isMedicare))
        {
            // delete the existing PQRI data for this appointment.
            $pqriProvider->deletePQRIForAppointment($pqriAptID, $pqriAddn);

            return $pqri;
        }

        if ($myMods->PQRI && ! $request->isSubmittedDraft())
        {
            $errorCountPreCheck = count($error);

            // If an exception is thrown here, don't change anything
            $pqriProvider->check (
                    $requestData, 
                    $error,
                    $patient,
                    $AptType,
                    $pqriAptID,
                    $myAppointment->Date,
                    $pqriAddn,
                    $myClinic,
                    $myUser, 
                    $myBillingSheet,
                    $AptID,
                    $request->getPqrsAnswers()
                );
            // If the check produces errors, clear the stored checksum to ensure a resubmit with the same errors doesn't
            // bypass a future check
            if(count($error) > $errorCountPreCheck){
                $this->setPqrsChecksum($myAppointment, '');
            }
        }

        // if it is a no charge visit then just exit. We can't record PQRS for no charge visits
        if ($myBillingSheet->isNoChargeVisit())
        {
            // delete the existing PQRI data for this appointment.
            $pqriProvider->deletePQRIForAppointment($pqriAptID, $pqriAddn);
        }
        else
        {
            //Update the PQRS data.
            $pqriProvider->recordData($recordableDto);
        }

        if ($this->isFinalizingBackPainNote($requestData, $pqriAddn))
        {
           $this->clearBackPainCountCache($pqriProvider, $myUser, $myClinic, $myAppointment);
        }

        return $pqri;
    }

    /**
     * @param Appointment $myAppointment
     * @param $AptID
     * @param bool $fn
     * @param $intOriginalAptID
     * @param $myCP
     * @return int
     */
    public function createPDF(Appointment $myAppointment, $AptID, $fn=false, $intOriginalAptID, &$myCP)
    {
        $render = $this->createNewSoapPdfRenderer();
        $result = $render->createSaveSoapPdf($myAppointment, $AptID, $fn, $intOriginalAptID, $myCP);
        if ($fn && $result === SoapPdfRenderer::PDF_CREATED) {
            $pdf = $render->getPdf() instanceof PDF ? $render->getPdf() : null;
            $this->triggerFinalizedPdfEvent($myAppointment, $AptID, $intOriginalAptID, $myCP, $pdf);
        } else {
            $this->getLogger()->warn(
                sprintf('Did not finalize PDF for [AptID=%s] with [fn=%s] [result=%s]', $AptID, $fn, $result)
            );
        }
        return $result;
    }

    /**
     * @return wptSession
     */
    protected function getSessionInstance()
    {
        return $this->getSession();
    }

    /**
     * @return Logger
     */
    protected function getLoggerInstance()
    {
        return Logger::getLogger('legacy.saveSOAP');
    }

    /**
     * @return Logger
     */
    private function getLogger()
    {
        if ( ! $this->logger)
        {
            $this->logger = Logger::getLogger(__CLASS__);
        }

        return $this->logger;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $facilityId
     * @return mixed
     */
    protected function getClinicPreferenceByFacilityId($facilityId)
    {
        $myCP = ClinicPreference::getByFacilityId($facilityId);
        return $myCP;
    }

    /**
     * @param $facilityId
     * @return mixed
     */
    protected function getClinicModulesByFacilityId($facilityId)
    {
        $myMods = ClinicModule::getByFacilityId($facilityId);
        return $myMods;
    }

    /**
     * @param $userId
     * @return mixed
     */
    protected function getTherapistById($userId)
    {
        $myUser = Therapist::getByID($userId);
        return $myUser;
    }

    /**
     * @param $facilityId
     * @return mixed
     */
    protected function getClinicByFacilityId($facilityId)
    {
        $myClinic = Clinic::getByID($facilityId);
        return $myClinic;
    }

    /**
     * @param $AptID
     * @return mixed
     */
    protected function getAppointmentById($AptID)
    {
        return Appointment::getById($AptID);
    }

    /**
     * @param $AptID
     * @return mixed
     */
    protected function getAppointmentByAddendumId($AptID)
    {
        return Appointment::getByAddendumId($AptID);
    }

    /**
     * @return Appointment
     */
    protected function getNewAppointmentInstance()
    {
        $myAppointment = new Appointment();
        return $myAppointment;
    }

    /**
     * @return Addendum
     */
    protected function getNewAddendumInstance()
    {
        $myAddendum = new Addendum ();
        return $myAddendum;
    }

    /**
     * @return Patient
     */
    protected function getNewPatientInstance()
    {
        $patient = new Patient ();
        return $patient;
    }

    /**
     * @return PTCase
     */
    protected function getNewPTCaseInstance()
    {
        $myPTCase = new PTCase ();
        return $myPTCase;
    }

    /**
     * @param $myAppointment
     * @return mixed
     */
    protected function createSubjectiveFromAppointment($myAppointment)
    {
        $mySubjective = Subjective::createFromAppointment($myAppointment);
        return $mySubjective;
    }

    /**
     * @param $myAppointment
     * @return mixed
     */
    protected function createAssessmentFromAppointment($myAppointment)
    {
        $myAssessment = Assessment::createFromAppointment($myAppointment);
        return $myAssessment;
    }

    /**
     * @param $myAppointment
     * @param $myUser
     * @return mixed
     */
    protected function createPlanFromAppointment($myAppointment, $myUser)
    {
        $myPlan = Plan::createFromAppointment($myAppointment, $myUser);
        return $myPlan;
    }

    /**
     * @return PatientPrecautions
     */
    protected function getNewPatientPrecautionsInstance()
    {
        $precautions = new PatientPrecautions ();
        return $precautions;
    }

    /**
     * @param $aptId
     * @param $patientId
     * @param $companyId
     * @param $teacherVer
     * @param $facilityId
     * @param bool $isDataFromLastAppointment
     * @param $aptType
     * @param bool $isAddendum
     * @return BillingSheet
     */
    protected function createBillingSheet($aptId, $patientId, $companyId, $teacherVer, $facilityId, $isDataFromLastAppointment = false, $aptType = null, $isAddendum = false)
    {
        $myBillingSheet = new BillingSheet ($aptId, $patientId, $companyId, $teacherVer, $facilityId, $isDataFromLastAppointment, $aptType, $isAddendum);
        return $myBillingSheet;
    }

    /**
     * @param $testId
     * @return individualTest
     */
    protected function buildTestFactory($testId)
    {
        $objMeasure131Test = TestFactory::build($testId);
        return $objMeasure131Test;
    }

    /**
     * @return dashAlerts
     */
    protected function createNewDashAlertsInstance()
    {
        $myAlert = new dashAlerts ();
        return $myAlert;
    }

    /**
     * @param $objReadOnlyDatabase
     * @return Authorization
     */
    protected function createNewAuthorizationWithDbInstance($objReadOnlyDatabase)
    {
        $myAuthorization = new Authorization ($objReadOnlyDatabase);
        return $myAuthorization;
    }

    /**
     * @param $finalize
     * @return bool
     */
    protected function fileExists($finalize)
    {
        return file_exists($finalize);
    }

    /**
     * @return PQRI
     */
    protected function createNewPqriInstance()
    {
        $pqri = new PQRI();
        return $pqri;
    }

    /**
     * @param $pqri
     * @param $facilityId
     * @return PQRIProvider
     */
    protected function createNewPqriProviderInstance($pqri, $facilityId)
    {
        $pqriProvider = new PQRIProvider($pqri, $facilityId);
        return $pqriProvider;
    }

    /**
     * @return Appointment
     */
    protected function createNewAppointmentInstance()
    {
        $objOriginalAppointment = new Appointment();
        return $objOriginalAppointment;
    }

    /**
     * @param $objReadOnlyDatabase
     * @param $myAppointment
     * @param $mySubjective
     * @param $myAssessment
     * @param $myPlan
     * @param $mySession
     * @param $myCP
     */
    protected function createSoapPlanOfCare($objReadOnlyDatabase, $myAppointment, $mySubjective, $myAssessment, $myPlan, $mySession, $myCP)
    {
        Soap::createPOC($myAppointment, $mySubjective, $myAssessment, $myPlan, $mySession, $myCP, $objReadOnlyDatabase);
    }

    /**
     * @param $facilityId
     * @return QuickDischargeBuilderUser
     */
    protected function createQuickDischargeSaveBuilder($facilityId)
    {
        $quickDischarge = QuickDischargeSaveBuilder::
            setFacilityId((int)$facilityId);
        return $quickDischarge;
    }

    /**
     * @return QuickDischargePdfBuilderAppointment
     */
    protected function createQuickDischargePdfBuilder()
    {
        return QuickDischargePdfBuilder::setFinalize(true);
    }

    /**
     * @return Snapshot
     */
    protected function createNewSnapshotInstance()
    {
        /**
         * @var $patientSnapshot PatientSnapshot
         */
        return StaticServiceManager::getInstance()->get("Wpt.Snapshot");
    }

    /**
     * @return \Wpt\SOAP\SoapPdfRenderer
     */
    protected function createNewSoapPdfRenderer()
    {
        if ($this->getServiceManager()->has('Wpt.SOAP.SoapPdfRenderer')) {
            return $this->getServiceManager()->get('Wpt.SOAP.SoapPdfRenderer');
        }
        $render = new \Wpt\SOAP\SoapPdfRenderer();
        return $render;
    }

    /**
     * @return Wpt_Cache
     */
    protected function getWptCacheInstance()
    {
        return Wpt_Cache::getInstance();
    }

    /**
     * @return Manager
     */
    protected function getFunctionalLimitationsManagerInstance()
    {
        return Manager::getInstance();
    }

    /**
     * @return DaoFactory
     */
    protected function getDaoFactoryInstance()
    {
        $doa = \Wpt\Persistence\DaoFactory::getInstance();
        return $doa;
    }

    /**
     * @return \ICache2
     */
    protected function getWptCache2Instance()
    {
        return Wpt_Cache2::getInstance();
    }

    /**
     * @return Wpt_Patient_Prescription
     */
    protected function getWptPatientPrescriptionInstance()
    {
        return Wpt_Patient_Prescription::getInstance();
    }

    /**
     * @param $string
     */
    protected function wptRedirect($string)
    {
        $this->logSoapEnd();

        Util::wptRedirect($string);
    }

    /**
     * @param $myAppointment
     * @return mixed
     */
    protected function getObjectiveByAppointment($myAppointment)
    {
        $myObjective = Objective::getByAppointment($myAppointment);
        return $myObjective;
    }

    /**
     * @param $myAppointment
     * @return mixed
     */
    protected function planOfCareStatusUpdateForAddendum($myAppointment)
    {
        if ($myAppointment->AptID < 0) {

            $pocSignatureStatus = new PlanOfCare();
            $appointmentID = $pocSignatureStatus->getAppointmentIdByAddendum($myAppointment->AptID);

            $result = $pocSignatureStatus->deleteSignatureStatusByAppointmentIdForAddendum($appointmentID);

            if($result){
                return true;
            }
        }

        return false;
    }

    /**
     * @param ServiceLocatorInterface $serviceManager
     */
    public function setServiceManager(ServiceLocatorInterface $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * @param $facilityId
     * @param $companyId
     * @param $clinicianType
     * @param $caseId
     * @return mixed
     */
    protected function saveVisitToFotoService($facilityId, $companyId, $clinicianType, $caseId)
    {
        return Visit::getInstance()->postVisit($facilityId, $companyId, $clinicianType, $caseId);
    }

    /**
     * @param $patientId
     * @param $facilityId
     * @param $companyId
     * @param $caseId
     * @return mixed
     */
    protected function saveDischargeToFotoService($patientId, $facilityId, $companyId, $caseId)
    {
        return Discharge::getInstance()->postDischarge($patientId, $facilityId, $companyId, $caseId);
    }

    /**
     * @param $patientId
     * @param $caseId
     * @param $facilityId
     * @return bool|string
     */
    protected function verifyFoto($patientId, $caseId, $facilityId)
    {
        /** @var FotoVerifyDto $verifyDto */
        $verifyDto =  Visit::getInstance()->verifyFotoForClinicAndPatient($patientId, $facilityId, $caseId);

        // For future development?  Cascading messages for unmet criteria to log to Foto
        if($verifyDto->getModuleEnabled()){
            if($verifyDto->getApiKeySet()) {
                if($verifyDto->getPatientRegistered() && $verifyDto->getPatientFotoId() !== ''){
                    if($verifyDto->getCaseClaimed()){
                        return true;
                    }
                    return FOTO::FOTO_VERIFY_EPISODE;
                }
                return Foto::FOTO_VERIFY_REGISTERED;
            }
            return Foto::FOTO_VERIFY_APIKEY;
        }
        return false;
    }


    /**
     * Criteria for logging visit or discharge with Foto
     * 1.  Must have FOTO module enabled (checked in service)
     * 2.  Must have FOTO api set (checked in service)
     * 3.  If logging patient visit, patient must have FOTO ID and show as registered (checked in service)
     * @param $requestData
     * @param $postData
     * @param wptSession $mySession
     * @param Appointment $myAppointment
     * @param $isAutoSave
     * @return bool
     *
     * @deprecated
     */
    protected function logAppointmentToFoto($requestData, $postData, $mySession, $myAppointment, $isAutoSave)
    {
        if(in_array($postData['submitted'], array('Draft', 'Preview', 'Forward')) || $isAutoSave || $myAppointment->AptID < 0) {
            return false;
        }

        $patientId = $requestData['ID'];
        $facilityId = $mySession->getFacilityID();
        $caseId = $requestData['CaseID'];

        $verified = $this->verifyFoto($patientId, $caseId, $facilityId);

        if ($verified === true) {

            $companyId = $mySession->getCompanyID();
            $clinicianType = $mySession->getInstance()->getUser()->getUserTypeDescription();
            $aptType = $myAppointment->getAptType();

            // Logging a visit.  Can only be DN or OD and not an addendum
            if (in_array($aptType, array( 'DN', 'OD' )) || $myAppointment->includesDailyNote() ) {
                $this->saveVisitToFotoService($facilityId, $companyId, $clinicianType, $caseId);
                return true;
            }

        }

        return false;
    }

    /**
     * @param $requestData
     * @param $postData
     * @param wptSession $mySession
     * @param Appointment $myAppointment
     * @param $isAutoSave
     * @return bool
     *
     * @deprecated
     */
    private function logAppointmentAsDischargeToFoto($requestData, $postData, $mySession, $myAppointment, $isAutoSave)
    {
        if(in_array($postData['submitted'], array('Draft', 'Preview', 'Forward')) || $isAutoSave || $myAppointment->AptID < 0) {
            return false;
        }

        $patientId = $requestData['ID'];
        $facilityId = $mySession->getFacilityID();
        $caseId = $requestData['CaseID'];

        $verified = $this->verifyFoto($patientId, $caseId, $facilityId);

        if ($verified === true) {
            $companyId = $mySession->getCompanyID();
            $this->saveDischargeToFotoService($patientId, $facilityId, $companyId, $caseId);
            return true;
        }

        return false;

    }

    /**
     * @param $appointment
     * @param $case
     */
    private function saveAppointmentDiagnosis($appointment, $case)
    {
        $diagnosisHistoryService = new Diagnosis();
        $diagnosisHistoryService->saveDiagnosis($appointment, $case);
    }

    /**
     * If an AddendumRequired record exists Update it with the addendum id.
     */
    private function saveAddendumRequiredAddendumId(Appointment $appointment)
    {
        if($appointment->getAppointmentID()<0){
            $service = new AddendumRequiredService();
            $service->setAddendumId(
                $appointment->getCaseID(),
                $appointment->getOriginalAppointmentId(),
                $appointment->getAddendumId()
            );
        }
    }

    /**
     * @return SoapService
     */
    protected function getSoapService()
    {
        return SoapService::getInstance();
    }

    /**
     * @return Wpt_Cache
     */
    public function getCache()
    {
        if ( ! $this->cache)
        {
            $this->cache = Wpt_Cache::getInstance();
        }

        return $this->cache;
    }

    /**
     * @param Wpt_Cache $cache
     * @return self
     */
    public function setCache(Wpt_Cache $cache)
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @return Wpt_Config
     */
    public function getConfig()
    {
        if ( ! $this->config)
        {
            $this->config = Wpt_Config::getInstance();
        }

        return $this->config;
    }

    /**
     * @param Wpt_Config $config
     * @return self
     */
    public function setConfig(Wpt_Config $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return wptSession
     */
    public function getSession()
    {
        if ( ! $this->session)
        {
            $this->session = wptSession::getInstance();
        }

        return $this->session;
    }

    /**
     * @param wptSession $session
     * @return self
     */
    public function setSession(wptSession $session)
    {
        $this->session = $session;
        return $this;
    }

    /**
     * @return CacheEntityManager
     */
    public function getCacheEntityManager()
    {
        if ( ! $this->cacheEntityManager)
        {
            $this->cacheEntityManager = CacheEntityManager::getInstance();
        }

        return $this->cacheEntityManager;
    }

    /**
     * @param CacheEntityManager $cacheEntityManager
     * @return self
     */
    public function setCacheEntityManager(CacheEntityManager $cacheEntityManager)
    {
        $this->cacheEntityManager = $cacheEntityManager;
        return $this;
    }

    /**
     * @return int
     */
    protected function getSaveSOAPChecksumTTL()
    {
        $config = $this->getConfig();
        return (int)$config->getConfig(self::SAVE_SOAP_CHECKSUM_TTL);
    }

    /**
     * This Dto replaces the old PQRIProvider->recordData signature
     * @see PQRIProvider::recordData
     *
     * @param mixed[] $requestData
     * @param Patient $patient
     * @param int $AptID
     * @param int $AddnID
     * @param string $serviceDate
     * @param Clinic $clinic
     * @param Therapist $user
     * @param BillingSheet $billingSheet
     * @return RecordablePqrsDataDto
     */
    protected function newRecordablePqrsDataDto($requestData, $patient, $AptID, $AddnID, $serviceDate, $clinic, $user, $billingSheet)
    {
        $dto = new RecordablePqrsDataDto();
        $dto->setRequestData($requestData);
        $dto->setPatient($patient);
        $dto->setAptId($AptID);
        $dto->setAddnId($AddnID);
        $dto->setServiceDate($serviceDate);
        $dto->setClinic($clinic);
        $dto->setUser($user);
        $dto->setBillingSheet($billingSheet);

        return $dto;
    }

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        if ( ! $this->serviceManager)
        {
            $this->serviceManager = StaticServiceManager::getInstance();
        }

        return $this->serviceManager;
    }

    /**
     * Process WebOutcomes Visit/Questionnaire and Create WebPT WO Document
     *
     * @param Appointment $myAppointment
     * @param Appointment|null $lastAppointment
     * @param array $requestData
     *
     * @return void
     */
    private function processWebOutcomesVisit(Appointment $myAppointment, $lastAppointment, array $requestData = [])
    {
        // Process WebOutcomes visit if note is a Evaluative Note Group, check isEvaluativeGroup for details
        if ($myAppointment->isEvaluativeGroup()) {
            if (!empty($requestData)) {
                /** @var WebOutcomesServiceWithMipsSupportInterface $webOutcomesService */
                $webOutcomesService = $this->getServiceManager()->get(
                    WebOutcomesServiceWithMipsSupportInterface::class
                );
                $webOutcomesService->updateForFinalizedAppointment(
                    $myAppointment,
                    $lastAppointment,
                    $requestData
                );
            } else {
                /** @var WebOutcomesService $WebOutcomesService */
                $webOutcomesService = $this->getServiceManager()->get('WebOutcomesService');
                $webOutcomesService->updateForFinalizedAppointment($myAppointment, $lastAppointment);
            }
        }
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (!$this->eventManager) {
            $this->eventManager = new EventManager(array(
                __CLASS__,
                get_called_class(),
            ));
            $serviceManager = $this->getServiceManager();
            if ($serviceManager->has($listenerServiceName = 'Wpt.Documentation.Event.Listener')) {
                $listenerAggregate = $serviceManager->get($listenerServiceName);
                $this->eventManager->attach($listenerAggregate);
            }
        }
        return $this->eventManager;
    }

    /**
     * @param Appointment $myAppointment
     * @param $AptID
     * @param $intOriginalAptID
     * @param $myCP
     * @param PDF $pdf
     */
    private function triggerFinalizedPdfEvent(
        Appointment $myAppointment,
        $AptID,
        $intOriginalAptID,
        &$myCP,
        PDF $pdf = null
    ) {
        $event = new Event(
            self::SOAP_NOTE_PDF_FINALIZED,
            $this,
            array(
                'appointment' => $myAppointment,
                'appointmentId' => $AptID,
                'isFinalized' => true,
                'originalAppointmentId' => $intOriginalAptID,
                'clinicPreferences' => $myCP,
                'pdf' => $pdf
            )
        );
        $this->getEventManager()->trigger($event);
    }

    private function logSoapEnd()
    {
        /**
         * @var \Wpt\Logging\Logger\SoapLogger $soapLogger
         */
        $soapLogger = StaticServiceManager::getInstance()->get('SoapLogger');
        $soapLogger->setCaseId($_REQUEST['CaseID']);
        $soapLogger->setPatientId($_REQUEST['ID']);
        $soapLogger->info('Save Soap End');
    }

    /**
     * Responsible to verify has change found.
     *
     * @param int $sessionId
     * @param Request $request
     *
     * @return int
     */
    private function hasChangeFound($sessionId, $request)
    {
        $autoSaveService = $this->getAutoSaveVerifierService();

        return $autoSaveService->hasChangesFound($sessionId, $request);
    }

    /**
     * Returns a AutoSaveVerifier service.
     *
     * @return AutoSaveVerifierInterface
     */
    public function getAutoSaveVerifierService()
    {
        if(!$this->autoSaveVerifierService){
            $this->autoSaveVerifierService = $this->getServiceManager()->get(AutoSaveVerifierInterface::class);
        }

        return $this->autoSaveVerifierService;
    }

    /**
     * Set a AutoSaveVerifier service.
     *
     * @param AutoSaveVerifierInterface $autoSaveVerifierService
     */
    public function setAutoSaveVerifierService($autoSaveVerifierService)
    {
        $this->autoSaveVerifierService = $autoSaveVerifierService;
    }

    /**
     * @return Patient
     */
    public function getPatientService()
    {
        if (!$this->patientService) {
            $this->patientService = $this->getNewPatientInstance();
        }

        return $this->patientService;
    }

    /**
     * @param Patient $patientService
     * @return void
     */
    public function setPatientService(Patient $patientService)
    {
        $this->patientService = $patientService;
    }

    /**
     * @param int $patientId
     * @param int $facilityId
     * @param int $caseId
     * @return Patient
     */
    private function loadPatient($patientId, $facilityId = null, $caseId = null)
    {
        $patient = $this->getPatientService();
        $patient->load($patientId, $facilityId, $caseId);

        return $patient;
    }

    /**
     * Define if there is not relation between T670 and PatientBilling.
     *
     * @param Appointment $mainAppointment
     * @param Appointment $appointment
     *
     * @return bool
     */
    private function hasErrorOnDataBase(Objective $objective, Appointment $appointment)
    {
        $foundError = false;

        if ($objective->hasTest('T670')) {
            $foundError = true;
            $patientBilling = PatientBilling::getCollectionByAppointmentID($appointment->getAppointmentID());
            
            if (!empty($patientBilling)) {
                /** @var PatientBilling $cptCode */
                foreach ($patientBilling as $cptCode) {
                    if (in_array($cptCode->getCPTSuffix(), ['CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN'], true)) {
                        $foundError = false;
                        break;
                    }
                }
            }
        }

        return $foundError;
    }

    /**
     * Write to Log data related with FLR Gcodes.
     */
    private function printLogFlrGcode()
    {
        $logger = $this->getLoggerInstance();
        $msgLog = $this->getFlrGcodesCollector()->getLogMessage();
        foreach ($msgLog as $log) {
            $logger->error($log);
        }
    }

    /**
     * Define if is there error regarding to Flr Gcopdes.
     */
    private function hasErrorOnFlrGcode()
    {
        /** @var Appointment $appointment */
        $appointment = $this->getAppointmentById($this->getFlrGcodesCollector()->getAppointmentId());
        if ($appointment instanceof Appointment) {
            $case = $appointment->getCase();
            if ($case instanceof PTCase) {
                $isFlr = $case->isFunctionalLimitationCase();
                if (
                    $this->getFlrGcodesCollector()->isFinalize() &&
                    $isFlr &&
                    $appointment->isDailyNoteGroup()
                ) {
                    $mainAppointment = $appointment->getOriginalNote();

                    if ($mainAppointment instanceof Appointment) {
                        if ($this->hasErrorOnDataBase($mainAppointment->getObjective(), $appointment)) {
                            $this->printLogFlrGcode();
                        }
                    }
                }
            }
        }
    }

    /**
     * Get instance of FLR collector.
     *
     * @return FlrGcodesCollector
     */
    private function getFlrGcodesCollector()
    {
        return FlrGcodesCollector::getInstance();
    }

    /**
     * Get Snapshot service used in save process.
     *
     * @return LegacyHarnessSaveHelperInterface|null
     */
    private function getSnapshotService()
    {
        return $this->getServiceManager()->get(LegacyHarnessSaveHelperInterface::class);
    }

    /**
     * @param $msg
     * @param Appointment $myAppointment
     * @param string $apptType
     *
     * @return boolean
     */
    private function canCreateQuickDischargePdf($msg, $myAppointment, $apptType)
    {
        return (null === $msg || 1 == $msg) &&  !$myAppointment->hasAddendum() && $apptType === $myAppointment->getAptType();
    }

    /**
     * Attaches a listener in Event manager class to enable react when a proper trigger is fired.
     *
     */
    private function attachPostSaveEventListener()
    {
        $serviceManager = $this->getServiceManager();
        if ($serviceManager->has(PostSaveEventListenerFactory::class)) {
            $listenerAggregate = $serviceManager->get(PostSaveEventListenerFactory::class);
            $this->getEventManager()->attach($listenerAggregate);
        }
    }

    /**
     * Fire an event when update data is required.
     *
     * @param Request $request
     */
    private function triggerPostSaveEvent(Request $request)
    {
        $event = new Event(
            BillableNoteListener::BILLABLE_NOTE_LISTENER,
            $this,
            [
                'request' => $request,
            ]
        );

        $this->getEventManager()->trigger($event);
    }

    /**
     * Get a PolicyAuthorizationService
     *
     * @return PolicyAuthorizationInterface
     */
    public function getPolicyAuthorizationService()
    {
        if (!$this->policyAuthorization) {
            $this->policyAuthorization = $this->getServiceManager()->get(PolicyAuthorizationInterface::class);
        }

        return $this->policyAuthorization;
    }

    /**
     * @param MipsData $mipsData
     * @return bool
     */
    private function isValidationMet(MipsData $mipsData)
    {
        $validator = $this->getMipsValidator();
        if ($validator !== null) {
            return $validator->isValid($mipsData);
        }

        return false;
    }

    /**
     * @return FeatureFlipInterface
     */
    public function getFeatureFlip()
    {
        if ($this->featureFlip === null && $this->getServiceManager()->has('FeatureFlip')) {
            $this->setFeatureFlip($this->getServiceManager()->get('FeatureFlip'));
        }

        return $this->featureFlip;
    }

    /**
     * @param FeatureFlipInterface $featureFlip
     */
    public function setFeatureFlip(FeatureFlipInterface $featureFlip)
    {
        $this->featureFlip = $featureFlip;
    }

    /**
     * @return ValidatorInterface | null
     */
    public function getMipsValidator()
    {
        if ($this->getServiceManager()->has(MipsValidatorsFactory::class)) {
            $this->setMipsValidator($this->getServiceManager()->get(MipsValidatorsFactory::class));
        }
        return $this->mipsValidator;
    }

    /**
     * @param ZendValidatorInterface $validator
     */
    public function setMipsValidator(ZendValidatorInterface $validator)
    {
        $this->mipsValidator = $validator;
    }

    /**
     * Check whether the measures to update have at least one HM measure in its list
     *
     * @param RecordablePqrsDataDto $recordableDto
     *
     * @return bool
     */
    private function hasValidHMMeasures(RecordablePqrsDataDto $recordableDto)
    {
        $hasValidMeasures = false;
        $measureUpdateList = $recordableDto->getMeasureUpdateList();

        if (count($measureUpdateList) > 0 && $periodAndMeasures = current($measureUpdateList)) {
            /** @var PQRIMeasure[] $pqriMeasures */
            $measures = $periodAndMeasures['measures'];

            if (!empty($measures) &&
                array_filter($measures, function (PQRIMeasure $measureToVerify) {
                    return $measureToVerify->isHMMeasure();
                })) {
                $hasValidMeasures = true;
            }
        }

        return $hasValidMeasures;
    }

    /**
     * @return bool
     */
    private function isSoapTestsPerformanceEnabled()
    {
        return $this->getFeatureFlip()->enabled('qnt-b25721-soap-tests-database-performance');
    }

    /**
     * @return SoapTestCacheCleanupInterface
     */
    public function getSoapTestCacheCleanupService()
    {
        if (!$this->soapTestCacheCleanupService instanceof SoapTestCacheCleanupInterface) {
            /** @var SoapTestCacheCleanupInterface $flushCacheService */
            $flushCacheService = $this->getServiceManager()->get(SoapTestCacheCleanupInterface::class);
            $this->setSoapTestCacheCleanupService($flushCacheService);
        }

        return $this->soapTestCacheCleanupService;
    }

    /**
     * @param SoapTestCacheCleanupInterface $soapTestFlushCacheService
     */
    public function setSoapTestCacheCleanupService(SoapTestCacheCleanupInterface $soapTestFlushCacheService)
    {
        $this->soapTestCacheCleanupService = $soapTestFlushCacheService;
    }

    /**
     * @return ZendValidatorInterface
     */
    public function getSoapFinalizationSurgeryValidator()
    {
        if (!$this->soapFinalizationSurgeryValidator instanceof ZendValidatorInterface) {
            /** @var ZendValidatorInterface $soapFinalizationSurgeryValidator */
            $soapFinalizationSurgeryValidator = $this->getServiceManager()->get(SoapFinalizationSurgeryValidator::class);
            $this->setSoapFinalizationSurgeryValidator($soapFinalizationSurgeryValidator);
        }

        return $this->soapFinalizationSurgeryValidator;
    }

    /**
     * @param ZendValidatorInterface $soapFinalizationSurgeryValidator
     */
    public function setSoapFinalizationSurgeryValidator(ZendValidatorInterface $soapFinalizationSurgeryValidator)
    {
        $this->soapFinalizationSurgeryValidator = $soapFinalizationSurgeryValidator;
    }

    /**
     * @return SoapValidationErrorInterface
     */
    public function getSoapValidationErrorService()
    {
        if (!$this->soapValidationErrorService instanceof SoapValidationErrorInterface) {
            /** @var ZendValidatorInterface $soapFinalizationSurgeryValidator */
            $soapValidationErrorService = $this->getServiceManager()->get(SoapValidationErrorService::class);
            $this->soapValidationErrorService = $soapValidationErrorService;
        }

        return $this->soapValidationErrorService;
    }

    /**
     * @return bool
     */
    private function isMovetoRoPayerQueryEnabled()
    {
        return $this->getFeatureFlip()->enabled('qnt-b30853-move-to-ro-payer-query');
    }

    /**
     * return bool
     */
    private function isCachePayerQueryEnabled()
    {
        return $this->getFeatureFlip()->enabled('qnt-b30882-cache-payer-query');
    }

    private function getSoapEventEmitter() {
        return $this->getServiceManager()->get('Wpt.Soap.SoapEventEmitter');
    }

    /**
     * Verifies if B-32960 FF qnt-b32960-audit-log-event-sent-create-soap is enabled.
     * @return bool
     */
    private function isAuditLogEventSentCreateSoapEnabled()
    {
        return $this->getFeatureFlip()->enabled('qnt-b32960-audit-log-event-sent-create-soap');
    }

    /**
     * Call and execute event if FF qnt-b32960-audit-log-event-sent-create-soap is enabled for create patient documents.
     * @param Appointment $appointment
     */
    private function emitNewDocument($appointment)
    {
        /** @var AppointmentProviderInterface $serviceCaseNote */
        $serviceCaseNote = $this->getServiceManager()->get(AppointmentProvider::class);
        $appointment = $serviceCaseNote->getAppointment(
            $appointment->getPatientID(),
            $appointment->getCaseID(),
            $appointment->getDocId()
        );

        $emitter = $this->getFinalizePatientNoteEmitterEvent();
        $emitter->emit($appointment);
    }

    /**
     * @return FinalizePatientNoteEventEmitterInterface
     */
    public function getFinalizePatientNoteEmitterEvent()
    {
        if (!$this->finalizePatientNoteEventEmitter instanceof FinalizePatientNoteEventEmitterInterface) {
            $this->setFinalizePatientNoteEmitterEvent(
                $this->getServiceManager()->get('Wpt.Soap.FinalizePatientNoteEventEmitter')
            );
        }

        return $this->finalizePatientNoteEventEmitter;
    }

    /**
     * @param FinalizePatientNoteEventEmitterInterface $finalizePatientNoteEventEmitter
     */
    public function setFinalizePatientNoteEmitterEvent(FinalizePatientNoteEventEmitterInterface $finalizePatientNoteEventEmitter)
    {
        $this->finalizePatientNoteEventEmitter = $finalizePatientNoteEventEmitter;
    }

    /**
     * @return bool
     */
    private function isEmrServiceDpiDataForPatientEnabled()
    {
        return $this->getFeatureFlip()->enabled('qnt-b34226-emr-service-dpi-data-for-patient');
    }

    /**
     * @return ProcessorInterface
     */
    public function getDpiUpdater()
    {
        if (!$this->dpiUpdater instanceof ProcessorInterface) {
            $this->setDpiUpdater($this->getServiceManager()->get(DigitalPatientIntakeUpdater::class));
        }

        return $this->dpiUpdater;
    }

    /**
     * @param ProcessorInterface $dpiUpdater
     */
    public function setDpiUpdater(ProcessorInterface $dpiUpdater)
    {
        $this->dpiUpdater = $dpiUpdater;
    }

    /**
     * @return CoCqBillingServiceInterface
     */
    public function getCoCqBillingService()
    {
        if (!$this->coCqBillingService instanceof CoCqBillingServiceInterface) {
            /** @var CoCqBillingServiceInterface $service */
            $service = $this->getServiceManager()->get(CoCqBillingService::class);
            $this->setCoCqBillingService($service);
        }

        return $this->coCqBillingService;
    }

    /**
     * @param CoCqBillingServiceInterface $coCqBillingService
     */
    public function setCoCqBillingService($coCqBillingService)
    {
        $this->coCqBillingService = $coCqBillingService;
    }

    /**
     * @return bool
     */
    private function isIromsExclusionEnabled()
    {
        return $this->getFeatureFlip()->enabled(FeatureFlipConstant::EXCLUSION_AND_EXCEPTIONS_QUESTION_FOR_IROMS);
    }

    /**
     * @return ZendValidatorInterface
     */
    public function getIromsExclusionRequiredValidator()
    {
        if (!$this->iromsExclusionRequiredValidator instanceof ZendValidatorInterface) {
            /** @var ZendValidatorInterface $iromsRequiredValidator */
            $iromsRequiredValidator = $this->getServiceManager()->get(IromsExclusionRequiredValidator::class);
            $this->setIromsExclusionRequiredValidator($iromsRequiredValidator);
        }

        return $this->iromsExclusionRequiredValidator;
    }

    /**
     * @param ZendValidatorInterface $iromsExclusionRequiredValidator
     */
    public function setIromsExclusionRequiredValidator(ZendValidatorInterface $iromsExclusionRequiredValidator)
    {
        $this->iromsExclusionRequiredValidator = $iromsExclusionRequiredValidator;
    }

    /**
     * @param Subjective|null $subjective
     * @param TherapistInterface|null $myUser
     * @param PQRIProvider|null $pqriProvider
     *
     * @return bool
     */
    private function isValidIromsExclusionQuestion($subjective, $myUser, $pqriProvider)
    {
        if (is_null($subjective) ||
            is_null($pqriProvider) ||
            is_null($pqriProvider->getCurrentPeriod()) ||
            is_null($pqriProvider->getCurrentPeriod()->getPeriod())) {
            return false;
        }

        $mipsData = new MipsData();
        $mipsData->setUser($myUser);
        $mipsValidationContext = new MIPSValidationContext();
        $mipsValidationContext->setMipsData($mipsData);
        $mipsValidationContext->setAppointment($subjective->getAppointment());
        $mipsValidationContext->setPeriod($pqriProvider->getCurrentPeriod()->getPeriod());

        return $this->getIromsExclusionValidator()->isValid($mipsValidationContext);
    }

    /**
     * @return IromsExclusionPersistenceInterface
     */
    public function getIromsExclusionPersistenceService()
    {
        if (!$this->iromsExclusionPersistenceService instanceof IromsExclusionPersistenceInterface) {
            /** @var IromsExclusionPersistenceInterface $service */
            $service = $this->getServiceManager()->get(IromsExclusionPersistenceService::class);
            $this->setIromsExclusionPersistenceService($service);
        }

        return $this->iromsExclusionPersistenceService;
    }

    /**
     * @param IromsExclusionPersistenceInterface $iromsExclusionPersistence
     */
    public function setIromsExclusionPersistenceService(IromsExclusionPersistenceInterface $iromsExclusionPersistence)
    {
        $this->iromsExclusionPersistenceService = $iromsExclusionPersistence;
    }

    /**
     * @return ZendValidatorInterface|null
     */
    public function getIromsExclusionValidator()
    {
        if (!$this->iromsExclusionValidator instanceof ZendValidatorInterface) {
            /** @var ZendValidatorInterface $validator */
            $validator  = $this->getServiceManager()->get('MIPS.IROMS.Exclusion.Validator');
            $this->setIromsExclusionValidator($validator);
        }

        return $this->iromsExclusionValidator;
    }

    /**
     * @param ZendValidatorInterface $iromsExclusionValidator
     */
    public function setIromsExclusionValidator(ZendValidatorInterface $iromsExclusionValidator)
    {
        $this->iromsExclusionValidator = $iromsExclusionValidator;
    }

    /**
     * @return AppointmentHasPayerServiceInterface
     */
    public function getAppointmentHasPayerService()
    {
        if (!$this->appointmentHasPayerService instanceof AppointmentHasPayerServiceInterface) {
            /** @var AppointmentHasPayerServiceInterface $service */
            $service = $this->getServiceManager()->get(AppointmentHasPayerService::class);
            $this->setAppointmentHasPayerService($service);
        }

        return $this->appointmentHasPayerService;
    }

    /**
     * @param AppointmentHasPayerServiceInterface $appointmentHasPayerService
     */
    public function setAppointmentHasPayerService(
        AppointmentHasPayerServiceInterface $appointmentHasPayerService
    ) {
        $this->appointmentHasPayerService = $appointmentHasPayerService;
    }

    private function isRemoveInsuranceRefPhysicianEnabled()
    {
        return $this->featureFlip->enabled(FeatureFlipConstant::REMOVE_INS_REF_PHYS_FROM_PDF);
    }

    /**
     * @param int $aptID
     */
    public function saveAptDraft($aptID)
    {
        $objAppointment = new Appointment();
        $objAppointment->load($aptID);
        if (isset($objAppointment->AptID)) {
            $objAppointment->saveAptDraft($aptID);
        }
    }
    
    /**
     * @param int $aptID
     */
    public function updateDnWithinIE($aptID)
    {
        $objAppointment = new Appointment();
        $objAppointment->load($aptID);
        if (isset($objAppointment->AptID)) {
            $objAppointment->updateDnWithinIE($aptID);
        }
    }

    private function isDnAndIeAreLinkingEnabled()
    {
        return $this->getFeatureFlip()
            ->enabled(FeatureFlipConstant::FIX_ERRORS_WHEN_CREATING_IE_WITH_DN);
    }
}
