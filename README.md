# THE BLOB ANTI-PATTERN

---
## A bit of context ...
>### _What is this anti pattern about?_
>
>The Blob is found in designs where one class monopolizes the processing, and other classes primarily encapsulate data. This AntiPattern is characterized by a class diagram composed of a single complex controller class surrounded by simple data classes. The key problem here is that the majority of the responsibilities are allocated to a single class.
>
